import os, re, functools, operator, itertools, time, sys
from copy import deepcopy
import numpy as np
import pandas as pd
import multiprocessing as mp

from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem.rdMolDescriptors import CalcNumAromaticRings, CalcNumRings

from osc_discovery.cheminformatics.cheminformatics_symmetry import Symmetry
from osc_discovery.cheminformatics.cheminformatics_structure_check import check_rules_fulfilled
from osc_discovery.cheminformatics.cheminformatics_misc import flatten_list, remove_numbering_wildcard_atom
from osc_discovery.morphing_typical_OSC_design import operations_typical_osc_design


""" Function to fully morph a molecule, see an example in scripts/generate_chemical_space.py """
            
def run_mutation(mol_last_generation_smiles, return_removed=False, verbose=False): 

    ''' Run a mutation on single molecule using SMARTS patterns from list of operations, returning all mutated structures '''
        
    # Morphing works by first applying an operation, 
    # second testing if molecule satifies symmetry criterion
    # and if not reapplication of operation.
    # Third, a cleanup is performed in which symmetry and validity are checked again.
        
    global new_generation, operations
    global failed, stopped, allow_only_graph_symmetric
    global mols_last_generation_smiles, preset_structure_check
    
    operations_dict = operations
    operations_list = list(operations.values())
        
    print('------------------')

    new_frames=[]
    removed=[]
    time_start=time.time()
    symm_checked_true=[]
    rules_checked_true=[]
    uniq=[]
    
    mol_last_gen=Chem.MolFromSmiles(mol_last_generation_smiles)
    
    if mol_last_gen == None: 
        print('Error, molecule invalid')
        return []
    
    Chem.SanitizeMol(mol_last_gen)
        
    
    count_tested=0
    for o,op in enumerate(operations_list):
                            
        try:
            rxn = AllChem.ReactionFromSmarts(op)
            ps = rxn.RunReactants((mol_last_gen,))           
            uniq = list(set([Chem.MolToSmiles(x[0]).split('.')[0] for x in ps]))
        except:
            os.system('echo "ERROR, first processing {} {}" >> sanitation_errors.txt'.format(mol_last_generation_smiles, op))
        
        if verbose:
            if len(uniq)==0:
                print('Failed', [list(operations_dict.keys())[i] for i,x in \
                                 enumerate(list(operations_dict.values())) if x==op])
            else: 
                print('Worked', [list(operations_dict.keys())[i] for i,x in \
                                 enumerate(list(operations_dict.values())) if x==op], uniq)
                
        if allow_only_graph_symmetric:
            
            uniq_new=[]
            for u in uniq:
                
                mol=Chem.MolFromSmiles(u, sanitize=False)
                Chem.SanitizeMol(mol, sanitizeOps=Chem.SanitizeFlags.SANITIZE_ALL^Chem.SanitizeFlags.SANITIZE_KEKULIZE)
                if mol==None: print('None'); continue
                
                try: 
                    symm=Symmetry()(mol)
                except: 
                    symm=False
                                
                if not symm:
                    if verbose: print('not symmetric',u)
                    try:
                        ps = rxn.RunReactants((mol,))
                        uniq2 = set([Chem.MolToSmiles(x[0]).split('.')[0] for x in ps])
                    except:
                        os.system('echo "ERROR, second symm processing {} {}" >> sanitation_errors.txt'.format(mol_last_generation_smiles, op))
                    if len(uniq2)==0: continue
                    uniq_new+=list(uniq2) 
            uniq+=list(set(uniq_new))
            
            
        count_tested+=len(set(uniq))
        for u in list(set(uniq)):
            
            try:
                
                mol=Chem.MolFromSmiles(u, sanitize=False)                
                err_Sanitize=Chem.SanitizeMol(mol,catchErrors=True)
                if err_Sanitize!=Chem.rdmolops.SanitizeFlags.SANITIZE_NONE:
                    os.system('echo "{} {} {} {}" >> sanitation_errors.txt'.format(err_Sanitize, mol_last_generation_smiles, op, u))
                    continue
     
                res_dict=''
                if allow_only_graph_symmetric:
                    symm_checker=Symmetry()
                    symm=symm_checker(mol)       
                    if not symm: continue
                    try:
                        symm_checker.res_partition 
                        res_dict = symm_checker.res_partition
                    except: pass

                if not check_rules_fulfilled(mol, res_dict, preset_structure_check=preset_structure_check): 
                    continue

                u = Chem.MolToSmiles(mol)
                    
            except: 
                
                failed+=1
                if verbose: print('Processing ERROR',u, mol_last_generation_smiles, op)
                os.system('echo "ERROR third processing {} {} {}" >> sanitation_errors.txt'.format(mol_last_generation_smiles, op, u))
                continue
                
            try: 
                u=Chem.MolToSmiles(Chem.MolFromSmiles(u))
            except: 
                continue
                
            new_frames.append(pd.DataFrame(data={
                                           'molecule_smiles': [u], 
                                           'operation': [op],
                                           'molecule_last_gen': [mol_last_generation_smiles], 
                                           'generation': [new_generation],
                                           'added_in_round': [new_generation]
                                           }))
                        
    if mol_last_generation_smiles in mols_last_generation_smiles:
        print('Time: {}, Mutation: {}, {}'.format(time.time()-time_start, 
                                                  mols_last_generation_smiles.index(mol_last_generation_smiles), 
                                                  mol_last_generation_smiles))

    if return_removed: 
        return new_frames, removed
        
    return new_frames



