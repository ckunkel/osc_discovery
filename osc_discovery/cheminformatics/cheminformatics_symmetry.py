import numpy as np
import time
from rdkit import Chem
from osc_discovery.cheminformatics.cheminformatics_misc import remove_numbering_wildcard_atom
from rdkit.Chem import rdMolDescriptors
from rdkit.Chem.rdmolfiles import CanonicalRankAtoms

    
# TODO: Go through and remove debug-statements and prints

    
class Symmetry():
    ''' Graph symmetry detection algorithm for molecular graph 
    
    Check if a molecular graph (2D) symmetry class can be identified. 

    Symmetry classes (see figure in manuscript):
    (1) full graph-symmetry, with all atomic environments appearing at least twice. 
        Atomic environments are thereby identified as similar if their subgraphs 
        extracted up to a bond radius of 3 are equivalent. This is very restrictive as 
        atoms that would lie on a single symmetry axis would be found non-symmetric. 
        Hence the two other symmetry classes are included. 
    (2) An asymmetric part in the molecule made of one or more fragments is symmetrically 
        substituted by an even number of similar fragments. E.g. B-A(E)-B, or C-B-A(E)-B-C 
    (3) a molecule is prosymmetric such that it has atomic sites on which a single substitution
        operation could lead to a molecule of class.
    '''
    
    def __init__(self): 
        pass
    
    def __call__(self, mol_rdkit, return_unsymmetric_fragment_info=False): 
        return self.check_symmetry(mol_rdkit, 
                                   return_unsymmetric_fragment_info=return_unsymmetric_fragment_info)
    
    def check_symmetry(self, mol_rdkit, return_unsymmetric_fragment_info=False):
        ''' Check if any type of symmetry can be found for the molecule. 
        
        Main method that tests all the subtypes (1)-(3)
        '''
        
        # Either fully graph-symmetric (1)
        if self._detect_graph_symmetry(mol_rdkit): return True

        # If the first condition was not given, we need to fragment the molecule
        # and carry the symmetry check out on this basis
    
        try:
            res_partition = self._partition_mol_fast(mol_rdkit)
            #res_partition = partition_mol_core_sg_linkers(mol_rdkit, biphenyl_cleavage=True, remove_sidegroups_from_cores=False)
            #if sorted(res_parition['bondids_pairs_cleaved'])==sorted(res_partition1['bondids_pairs_cleaved']):
            #    print(Chem.MolToSmiles(mol_rdkit))
        except:
            print( 'partition_mol', Chem.MolToSmiles(mol_rdkit) )
        
        self.res_partition = res_partition
        
        fragments = res_partition['cores']+res_partition['linkers']   
        fragments_smiles = [remove_numbering_wildcard_atom(Chem.MolToSmiles(x)) for x in fragments]
        set_frag_smiles=list(set(fragments_smiles))
        counts=np.array([fragments_smiles.count(x) for x in set_frag_smiles])

        # Is the molecule comprised of a single fragment and probably prosymmetric (3)?
        if list(counts)==[1]: 
            if self._detect_prosymmetry(mol_rdkit): return True
            else: return False
            
        else:  # Is the fragment symmetrically substituted (2)?
            time_start1=time.time()
            red1 = self._quick_check_reduced_graph_symmetry(mol_rdkit, res_partition)
            time_start1=time.time()-time_start1
            return red1
            #if not red1: return False
            time_start=time.time()
            red = self._check_reduced_graph_symmetry(mol_rdkit, res_partition,
                 return_unsymmetric_fragment_info=return_unsymmetric_fragment_info) 
            if red1!=red: print('1', red1, '2', red, Chem.MolToSmiles(mol_rdkit), time_start1, time_start-time.time())
            #print('1', red1, '2', red, Chem.MolToSmiles(mol_rdkit), time_start1, time_start-time.time())
            return red1
        
        
     
    def _quick_check_reduced_graph_symmetry(self, mol_rdkit, res_partition, 
                                  return_unsymmetric_fragment_info=False):

        ''' Detection of symmetric substitution. See description of the more exhaustive method
            Here this is just a quick precheck'''   

        # Partition mol and count occurence of fragments
        fragments = res_partition['cores']+res_partition['linkers']   
        fragments_smiles = [remove_numbering_wildcard_atom(Chem.MolToSmiles(x)) for x in fragments]
        fragments_atom_ids = res_partition['cores_atoms_ids']+res_partition['linkers_atoms_ids']

        bonds = [ (b.GetBeginAtomIdx(), b.GetEndAtomIdx(), b.GetIdx()) for b in mol_rdkit.GetBonds() ]
        for fids in fragments_atom_ids:
            bonds_cleave=[]
            bonds_idx_cleave=[]
            in_frag=[]
            for b in bonds:
                if b[0] in fids and not b[1] in fids: 
                    bonds_cleave.append(b[2]); in_frag.append(b[0]); bonds_idx_cleave.append((b[0],b[1],b[2]))
                elif b[1] in fids and not b[0] in fids: 
                    bonds_cleave.append(b[2]); in_frag.append(b[1]); bonds_idx_cleave.append((b[0],b[1],b[2]))
            if bonds_cleave==[]: continue

            frags=Chem.FragmentOnBonds(mol_rdkit, bonds_cleave)
            frags_ids_new = Chem.GetMolFrags(frags)
            
            # lets check if there is a plausible chance for symmetry
            central_cleaved_id = [i for i,x in enumerate(frags_ids_new) if set(in_frag).issubset(set(x))][0]
            frag_ids_central = frags_ids_new[central_cleaved_id]
            frags_ids_new = [x for i, x in enumerate(frags_ids_new) if not i==central_cleaved_id]
            lens_frags = [len(x) for i,x in enumerate(frags_ids_new)]
            counts_len_frags = [lens_frags.count(x) for x in lens_frags]
            
            
            # Now we are working our way to down to more expensive checkups, 
            # discarding unlikely cases which saves computation time
            # 1) layer, one asymmetric frag allowed, check if counts ok
            count1=counts_len_frags.count(1)
            #print(counts_len_frags)
            already_found_single=False
            index_1_count_unsymm=-1
            if count1<=1:
                if count1==1:
                    index_1_count_unsymm=counts_len_frags.index(1)
                    counts_len_frags.pop(index_1_count_unsymm)
                    frags_ids_new.pop(index_1_count_unsymm)
                    already_found_single=True
                    
                # 2) layer, check if smiles ok, fragments very likely can have similar counts, but actually different frags
                if len(counts_len_frags)>0 and np.all(np.array(counts_len_frags)>1):
                    #print(counts_len_frags)
                    new_mols = list(Chem.GetMolFrags(frags, asMols=True))
                    new_mols_central = new_mols[central_cleaved_id]
                    new_mols = [x for i,x in enumerate(new_mols) if not i==central_cleaved_id ]
                    if index_1_count_unsymm>-1:
                        mol_frag_unsymm = new_mols[index_1_count_unsymm]
                        new_mols.pop(index_1_count_unsymm)
                        if len([x for x in mol_frag_unsymm.GetAtoms() if not x.GetSymbol()=='*'])>9: 
                            continue
                    
                    bid_on_central=[]
                    for i,fid in enumerate(frags_ids_new):
                        for i,b in enumerate(bonds_idx_cleave):
                            if b[0] in fid or b[1] in fid: bid_on_central.append(b[2])                    

                    new_mols_smiles=[remove_numbering_wildcard_atom(Chem.MolToSmiles(x)) for i,x in enumerate(new_mols)]

                    counts_len_frags_smis = [new_mols_smiles.count(x) for x in new_mols_smiles]
                    count1=counts_len_frags_smis.count(1)  
                    #print(new_mols_smiles, counts_len_frags_smis)
                    if count1 <=1 :
                        if count1==1:
                            if already_found_single: continue
                            idx1=counts_len_frags_smis.index(1)
                            mol_frag_unsymm = new_mols[idx1]
                            if len([x for x in mol_frag_unsymm.GetAtoms() if not x.GetSymbol()=='*'])>9: 
                                continue
                            counts_len_frags_smis.pop( idx1 )
                            new_mols.pop( idx1 )
                            new_mols_smiles.pop(idx1)
                            frags_ids_new.pop(idx1)
                            bid_on_central.pop(idx1)

                        #print(new_mols_smiles)
                        # 3) layer, check if also the attachement points with the central fragment are ok
                        if len(counts_len_frags_smis)>0 and np.all(np.array(counts_len_frags_smis)>1): 
                                                        
                            frag_ids_central_no_attach = frag_ids_central[0:-len([at for at in \
                                             new_mols_central.GetAtoms() if at.GetSymbol()=='*'])]
                            
                            new_mols_smiles_cleaved=[]
                            
                            # now we need to break bonds respectively, one at a time and get frags
                            for b in bid_on_central:
                                frags_cleaved=Chem.FragmentOnBonds(mol_rdkit, [b])
                                frags_ids_new_cleaved = Chem.GetMolFrags(frags_cleaved)

                                new_mols_cleaved = list(Chem.GetMolFrags(frags_cleaved, asMols=True))
                                new_mols_smiles_cleaved+=[remove_numbering_wildcard_atom(Chem.MolToSmiles(x)) for i,x in \
                                  enumerate(new_mols_cleaved) if \
                                  set(frag_ids_central_no_attach).issubset(frags_ids_new_cleaved[i]) ]

                            counts_len_frags_smis = [new_mols_smiles_cleaved.count(x) for x in set(new_mols_smiles_cleaved)]
                            if len(counts_len_frags_smis)>0 and np.all(np.array(counts_len_frags_smis)>1): 
                                return True
                                

        return False


            
    def _detect_graph_symmetry(self, mol, radius_fp_min=5):
        ''' Detect graph symmetry, meaning if all heavy-atom environments occur at least twice. 
        
        Equivalent sites are thereby identified as sites with a similar atomic environment, 
        extracted up to a bond radius of 3.
        '''

        descs=[]
        for at in mol.GetAtoms():
            desc=rdMolDescriptors.GetMorganFingerprint(mol, radius_fp_min, fromAtoms=[at.GetIdx()] ).GetNonzeroElements() 
            str_desc=''
            for d in desc.keys():
                str_desc+=str(d)
            descs.append(str_desc)

        descs_set=list(set(descs))
        lens=[descs.count(x) for x in descs_set]
        if np.any(np.array(lens)<2): return False

        return True
    

    def _detect_prosymmetry(self, mol_rdkit):

        ''' Check if at least one pair of C atoms with at least one implicit H occur, that can be symmetrically substituted..
        
        Equivalent sites are thereby identified as sites with a similar atomic environment, 
        extracted up to a bond radius of 3.
        
        Note: It is assumed, that the supplied molecule consists of one fragment only. '''
        
        descs=[]
        symbols_implicit_hs=[]
        for at in mol_rdkit.GetAtoms():
            desc=rdMolDescriptors.GetMorganFingerprint(mol_rdkit, 5, fromAtoms=[at.GetIdx()] ).GetNonzeroElements() 
            str_desc=''
            for d in desc.keys():
                str_desc+=str(d)
            descs.append(str_desc)
            symbols_implicit_hs.append([at.GetSymbol(), at.GetNumImplicitHs()])

        lens=np.array([descs.count(x) for x in descs])
        if np.any(lens==2): 
            idxs=np.where(np.array(lens)==2)[0]
            selection=np.array(symbols_implicit_hs)[idxs]
            for sel in selection: 
                if sel[0] =='C' and int(sel[1])>=1:
                    return True
                
        return False   

    
    
    def _check_reduced_graph_symmetry(self, mol_rdkit, res_partition, 
                                      return_unsymmetric_fragment_info=False):

        ''' Detection of symmetric substitution.
        
        Central fragments in the molecule that potentially are symmetrically substitued are 
        bonded to other fragments on more than 1 site, i.e. sth like:
        B-A(E)-B, or C-B-A(E)-B-C.
        
        From those central fragments, the connections to all 
        others are cleaved one by one, and the resulting smiles string is recorded. 
        If every resulting smiles string is then found at least two times, the fragment central fragment
        was symmetrically substituted by our definition.        
        In cases where the number of substitution sites on the central fragment is odd, 
        substitution by a single fragment of at maximum 10 atoms, without loosing the symmetry assignment. 
        The limits are necessary to have a large part of the molecule symmetric. '''   
        
        # Partition mol and count occurence of fragments
        fragments = res_partition['cores']+res_partition['linkers']   
        fragments_smiles = [remove_numbering_wildcard_atom(Chem.MolToSmiles(x)) for x in fragments]
        fragments_smiles_cores = [remove_numbering_wildcard_atom(Chem.MolToSmiles(x)) \
                          for x in res_partition['cores']]
        fragments_smiles_linkers = [remove_numbering_wildcard_atom(Chem.MolToSmiles(x)) \
                          for x in res_partition['linkers']]
        fragments_atom_ids = res_partition['cores_atoms_ids']+res_partition['linkers_atoms_ids']

        set_frag_smiles=list(set(fragments_smiles))
        counts=np.array([fragments_smiles.count(x) for x in set_frag_smiles])

        # 1) find the uneven fragments
        frags_candidates_uneven_idxs = [i for i,x in enumerate(fragments_smiles) if x.count('*') > 1 ]
        
        # 2) Check, if it is symmetrically substituted
        symms=[]
        bonds_cleaved_symmetry_fragments=[]
        bonds_cleaved_unsymmetric_fragments=[]
                
        for frag_candidate_uneven_idx in frags_candidates_uneven_idxs:#

            # First, find all substitution sites and bonds that connect other fragments 
            # with the unsymmetric one. 
            frag_candidate_uneven_smi=fragments_smiles[frag_candidate_uneven_idx]
            frag_candidate_uneven = fragments[frag_candidate_uneven_idx]
            frag_atom_ids = fragments_atom_ids[frag_candidate_uneven_idx]
            possible_cleavages = res_partition['atomids_pairs_cleaved']
            substitution_sites=[]
            bonds_cleave=[]
            for b in possible_cleavages:
                if b[0] in frag_atom_ids:
                    substitution_sites.append(b[0])
                    bonds_cleave.append(mol_rdkit.GetBondBetweenAtoms(b[0],b[1]).GetIdx())
                if b[1] in frag_atom_ids:
                    substitution_sites.append(b[1])
                    bonds_cleave.append(mol_rdkit.GetBondBetweenAtoms(b[0],b[1]).GetIdx())
            frags_smiles_substitutions=[]
            frags_smiles_sites=[]
            hashes_subst_sites=[]

            # Cleave all bonds accordingly
            for i,b in enumerate(bonds_cleave):
                
                desc=rdMolDescriptors.GetMorganFingerprint(mol_rdkit, 1, fromAtoms=[substitution_sites[i]] ).GetNonzeroElements() 
                str_desc=''
                for d in desc.keys(): str_desc+=str(d)
                hashes_subst_sites.append(str_desc)
                
                frags_ids_new = Chem.GetMolFrags(Chem.FragmentOnBonds(mol_rdkit, [b]))
                new_mols = Chem.GetMolFrags(Chem.FragmentOnBonds(mol_rdkit, [b]), asMols=True)
                new_mols_smiles=[remove_numbering_wildcard_atom(Chem.MolToSmiles(x)) for x in new_mols]
                frags_smiles_substitutions.append([x for j,x in enumerate(new_mols_smiles) if not substitution_sites[i] in frags_ids_new[j]][0])
                frags_smiles_sites.append([x for j,x in enumerate(new_mols_smiles) if substitution_sites[i] in frags_ids_new[j]][0])
            
            
            # one uneven substitution is allowed
            if len(frags_smiles_sites)%2==1:
                unsymm_too_long=False
                for i, f in enumerate(frags_smiles_substitutions):
                    if frags_smiles_substitutions.count(f)==1:
                        if Chem.MolFromSmiles(f).GetNumAtoms()>10:
                            unsymm_too_long=True
                            break
                        frags_smiles_substitutions.pop(i)
                        frags_smiles_sites.pop(i)
                        hashes_subst_sites.pop(i)
                        bonds_cleaved_unsymmetric_fragments.append(bonds_cleave[i])
                        bonds_cleave.pop(i)
                        break
                if unsymm_too_long: symms.append(False); continue   

            smi_frag_symm_core = list(set(frags_smiles_sites))[0]
            
            for i,f in enumerate(frags_smiles_sites):
                frags_smiles_sites[i]=hashes_subst_sites[i]+'_'+f
                # we could also use hashes_subst_sites[i] instead of f

            # Now check, if the remaining sites are all larger 1
            counts_frags_sites=[frags_smiles_sites.count(f) for f in set(frags_smiles_sites)]
            if np.all(np.array(counts_frags_sites)>1):
                symms.append(True)
                bonds_cleaved_symmetry_fragments.append(bonds_cleave)
            else:
                symms.append(False)

        if np.sum(symms)>0: 
            if return_unsymmetric_fragment_info: 
                smi_core, smi_symm, symm_mol = self._get_unsymmetric_fragment(mol_rdkit, 
                                       bonds_cleaved_symmetry_fragments[0])
                # If the symmetric fragment assignment will stay securely. 
                if symm_mol.GetNumAtoms()>11:
                    return True, smi_symm, smi_frag_symm_core, smi_core
            return True
        return False
    
    
    def _get_unsymmetric_fragment(self, mol_rdkit, bonds_cleaved_symmetry_fragments):
        
        frags_ids_new = Chem.GetMolFrags(Chem.FragmentOnBonds(mol_rdkit, 
                                                              bonds_cleaved_symmetry_fragments))
        new_mols = Chem.GetMolFrags(Chem.FragmentOnBonds(mol_rdkit, 
                                                         bonds_cleaved_symmetry_fragments), 
                                                         asMols=True)
        new_mols_smiles=[remove_numbering_wildcard_atom(Chem.MolToSmiles(x)) for x in new_mols]
        smi_core = [x for x in new_mols_smiles if new_mols_smiles.count(x)==1][0]
        smi_symm = [x for x in new_mols_smiles if new_mols_smiles.count(x)!=1][0]
        symm_mol = [new_mols[i] for i,x in enumerate(new_mols_smiles) if new_mols_smiles.count(x)!=1][0]
        
        return smi_core, smi_symm, symm_mol
               
    
    def _partition_mol_fast(self,mol):

        ''' Fast version to partition a molecule '''

        pat = Chem.MolFromSmarts('[*]-[#6;R]')
        atoms_cleaved_at = [sorted(x) for x in mol.GetSubstructMatches(pat)]
        if len(atoms_cleaved_at)==0: 
            return {'cores': [mol],
             'cores_atoms_ids': [list(range(mol.GetNumAtoms()))],
             'linkers': [],
             'linkers_atoms_ids': [],
             'sidegroups': [],
             'sidegroups_atoms_ids': [],
             'atomids_pairs_cleaved': [],
             'bondids_pairs_cleaved': []}
        
        bonds_cleave = [mol.GetBondBetweenAtoms(at[0], at[1]) for at in atoms_cleaved_at]
        bonds_cleave = [b for b in bonds_cleave if not b.IsInRing()]
        atoms_cleaved_at2 = [(b.GetBeginAtomIdx(), b.GetEndAtomIdx()) for b in bonds_cleave]
        bonds_cleave = [b.GetIdx() for b in bonds_cleave]
            

        if len(atoms_cleaved_at2)==0: 
            return {'cores': [mol],
             'cores_atoms_ids': [list(range(mol.GetNumAtoms()))],
             'linkers': [],
             'linkers_atoms_ids': [],
             'sidegroups': [],
             'sidegroups_atoms_ids': [],
             'atomids_pairs_cleaved': [],
             'bondids_pairs_cleaved': []}


        frag_mol = Chem.FragmentOnBonds(mol, bonds_cleave)
        frags_ids_new = Chem.GetMolFrags(frag_mol)
        new_mols = Chem.GetMolFrags(frag_mol, asMols=True)

        resdict={}
        resdict['cores'] = []
        resdict['cores_atoms_ids'] = []
        resdict['linkers'] = []
        resdict['linkers_atoms_ids'] = []
        resdict['sidegroups'] = []
        resdict['sidegroups_atoms_ids'] = []
        resdict['atomids_pairs_cleaved'] = atoms_cleaved_at2
        resdict['bondids_pairs_cleaved'] = bonds_cleave

        count_connectors_list = [Chem.MolToSmiles(m).count('*') for m in new_mols]
        ring_infos = [m.GetRingInfo().AtomRings() for m in new_mols]

        for i,m in enumerate(new_mols):
            if count_connectors_list[i]==1: # can be sidegroup or core
                if len(ring_infos[i]) > 0: #core
                    resdict['cores'].append(new_mols[i])
                    resdict['cores_atoms_ids'].append(frags_ids_new[i])
                else:
                    resdict['sidegroups'].append(new_mols[i])
                    resdict['sidegroups_atoms_ids'].append(frags_ids_new[i])

            if count_connectors_list[i]>1: # can be linker or core
                if len(ring_infos[i]) > 0: #core
                    resdict['cores'].append(new_mols[i])
                    resdict['cores_atoms_ids'].append(frags_ids_new[i])
                else: #linker
                    resdict['linkers'].append(new_mols[i])
                    resdict['linkers_atoms_ids'].append([x for x in frags_ids_new[i] if x<mol.GetNumAtoms()])

        return resdict
    
