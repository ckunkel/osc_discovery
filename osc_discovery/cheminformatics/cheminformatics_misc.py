import re
from ase import Atoms
import numpy as np
import ase

try: import StringIO as io
except ImportError: import io
import openbabel.pybel as pd
    
""" Helpers to handle simple cheminformatics tasks """

    
def remove_numbering_wildcard_atom(smi_str):
    """ A cheminformatics helper that e.g. converts [3*] to [*], useful when working with RDKit functions """
    smi_str=re.sub("^[\*^]", "[*]", smi_str)#sometimes only * instead of [*] occurs
    return re.sub("\[[0-9]*\*\]", "[*]", smi_str)


def flatten_list(nested_list):
    """Flatten an arbitrarily nested list, without recursion (to avoid
    stack overflows). Returns a new list, the original list is unchanged.
    >> list(flatten_list([1, 2, 3, [4], [], [[[[[[[[[5]]]]]]]]]]))
    [1, 2, 3, 4, 5]
    >> list(flatten_list([[1, 2], 3]))
    [1, 2, 3]
    From https://gist.github.com/Wilfred/7889868
    """
    nested_list = deepcopy(nested_list)

    while nested_list:
        sublist = nested_list.pop(0)

        if isinstance(sublist, list):
            nested_list = sublist + nested_list
        else:
            yield sublist
            
            
def rdkit2ase(rdkit_mol,confId=0):
    """ Extract 3D coordinates from RDKit mol and convert to ASE mol """
    positions=rdkit_mol.GetConformer(confId).GetPositions()
    atoms_symbols=np.array([at.GetSymbol() for at in rdkit_mol.GetAtoms()])
    return Atoms(atoms_symbols, positions=positions)


def ase2xyz(atoms):
    """ Prepare a XYZ string from an ASE atoms object. """
    if any(atoms.get_pbc()):
        raise RuntimeError("Detected PBCs. Not supported (yet)!")
    num_atoms = len(atoms)
    types = atoms.get_chemical_symbols()
    all_atoms = zip(types, atoms.get_positions())
    a_str = str(num_atoms) + "\n" + "\n"
    for atom in all_atoms:
        a_str += atom[0] + " " + " ".join([str(x) for x in atom[1]]) + "\n"
    return a_str


def xyz2ase(xyz_str):
    """ Convert a xyz file to an ASE atoms object via in-memory file (StringIO). """
    xyzfile = io.StringIO()
    xyzfile.write(xyz_str)
    mol = ase.io.read(xyzfile, format="xyz")
    return mol


def ase2rdkit(atoms, removeHs=False):
    """ Convert an ASE atoms object to rdkit molecule.
    The ordering of the Atoms is identical."""
    a_str = ase2xyz(atoms)
    pymol = pb.readstring("xyz", a_str)
    mol = pymol.write("mol")
    mol = Chem.MolFromMolBlock(mol, removeHs=removeHs)
    return mol
