import functools, operator, itertools
from rdkit import Chem
from rdkit.Chem.rdMolDescriptors import CalcNumRings
from osc_discovery.cheminformatics.cheminformatics_symmetry import Symmetry

""" Routines that check for molecules if they fulfill the bounds set by the user """

def get_ids_atoms_in_rings(mol_rdkit):
    ''' Get ids of atoms that reside in closed rings '''
    atom_id_lists_rings=[]
    ssr = Chem.GetSymmSSSR(mol_rdkit)
    for s in list(ssr):
        atom_id_lists_rings.append(list(s))
    return atom_id_lists_rings


def get_num_heteroatoms(mol_rdkit, only_count_heteroatoms_in_rings=True):
    ''' Count Heteroatoms (other than C,H) in structure '''
    if only_count_heteroatoms_in_rings: 
        atom_id_lists = get_ids_atoms_in_rings(mol_rdkit)
        listidx = functools.reduce(operator.concat, atom_id_lists)
    else: listidx = range(mol_rdkit.GetNumAtoms())
    num_het=0
    for idx in listidx: #list(set(np.array(atom_id_lists_rings).flatten())):
        at=mol_rdkit.GetAtomWithIdx(idx).GetSymbol()
        if at != 'H' and at != 'C':
            num_het+=1
    return num_het


def get_non_ring_atoms(mol_rdkit):
    ''' Find all carbon atoms in linkers '''
    linker_atom_idx=[]
    for at in mol_rdkit.GetAtoms():
        if not at.IsInRing(): # or not at.GetIsAromatic():
            linker_atom_idx.append(at.GetIdx())
    return linker_atom_idx


def check_rules_fulfilled(mol_rdkit, res_dict='', preset_structure_check='', verbose=False, 
                           check_biphenyl_ring_systems=True,
                           only_count_heteroatoms_in_rings=False):
    
    ''' Check if molecule fulfills limits on molecular structure '''
    
    preset=preset_structure_check
    max_num_atoms_allowed=0

    
    if preset=='test_osc_space':
        # settings for H,C,N,O,S network for tests
        num_rings_allowed = 4
        num_linkers_allowed = 2
        num_core_frags_allowed = 5 # Maxmimum number of separate ring systems (separated by linkers or biphenylic bonds)
        max_num_rings_in_corefrag_allowed = 5 # maximum number of rings in a single annelated (core) ring system
        num_het_atoms_allowed = 4
        num_sidegroups_allowed = 4
        num_atoms_allowed = -1
    
    # unlimited space
    if preset=='' or  preset=='unlimited_osc_space':
        num_rings_allowed = -1
        num_linkers_allowed = -1
        num_core_frags_allowed = -1
        max_num_rings_in_corefrag_allowed = -1
        num_het_atoms_allowed = -1
        num_sidegroups_allowed = -1
        num_atoms_allowed = 100
    
    num_atoms = Chem.AddHs(mol_rdkit).GetNumAtoms()
    if verbose: print('Number of atoms allowed/found: {}/{}'.format(num_atoms_allowed, num_atoms))
    if num_atoms_allowed > -1 and not num_atoms <= num_atoms_allowed:  
        if verbose: print('too many atoms', num_atoms)
        return False
    
    num_rings = CalcNumRings(mol_rdkit)
    if verbose: print('Number of rings allowed/found: {}/{}'.format(num_rings_allowed, num_rings))
    if num_rings_allowed > -1 and not num_rings <= num_rings_allowed:  
        if verbose: print('too many rings', num_rings)
        return False
    
    if res_dict=='':
        res_dict=Symmetry()._partition_mol_fast(mol_rdkit)
    
    num_sidegroups = len(res_dict['sidegroups'])
    if verbose: print('Number of sidegroups (in core) allowed/found: {}/{}'.format(num_sidegroups_allowed, num_sidegroups))
    if num_sidegroups_allowed > -1 and not num_sidegroups <= num_sidegroups_allowed : 
        if verbose: print('too many sidegroups', num_sidegroups)
        return False
    
    num_het_atoms = get_num_heteroatoms(mol_rdkit, only_count_heteroatoms_in_rings=False)
    if verbose: print('Number of heteroatoms allowed/found: {}/{}'.format(num_het_atoms_allowed, num_het_atoms))
    if num_het_atoms_allowed > -1 and not num_het_atoms <= num_het_atoms_allowed: 
        if verbose: print('too many heteroatoms', num_het_atoms)
        return False
    
    num_linkers = len(res_dict['linkers'])
    if verbose: print('Number of linkers allowed/found: {}/{}'.format(num_linkers_allowed, num_linkers))
    if num_linkers_allowed > -1 and not num_linkers <= num_linkers_allowed: 
        if verbose: print('too many linkers', num_non_ring_atoms)
        return False

    num_core_frags=len(res_dict['cores'])
    if verbose: print('Number of core fragments allowed/found: {}/{}'.format(num_core_frags_allowed, num_core_frags))
    if num_core_frags_allowed > -1 and not num_core_frags <= num_core_frags_allowed:
        if verbose: print('Too many core fragments in the molecule')
        return False

    max_num_rings_in_corefrag = max([CalcNumRings(x) for x in res_dict['cores']])
    if verbose: print('Maximum number of rings in single core fragment allowed/found: {}/{}'.format(max_num_rings_in_corefrag_allowed, max_num_rings_in_corefrag))
    if max_num_rings_in_corefrag_allowed > -1 and not max_num_rings_in_corefrag <= max_num_rings_in_corefrag_allowed:
        if verbose: print('A core fragment contains too many rings')
        return False
            
    return True
