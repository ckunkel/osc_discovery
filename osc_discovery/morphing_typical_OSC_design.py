from copy import deepcopy

operations_typical_osc_design_ch = {
    
    'Biphenyl addition': '[cH&r6,CH&r5:1]>>[#6:1](-[c]1[ch]ccc[ch]1)',  #1
    'reverse_Biphenyl addition': '[c&r6,C&r5:1](-[c]1[ch]ccc[ch]1)>>[*:1]',   #2
    
    '6-ring annelation':'[cH&r6:1][cH&r6:2]>>[c:1]2cccc[c:2]2',  #3
    'reverse_6-ring annelation':'[r6:0][c:1]2[ch][ch][ch][ch][c:2]2[r6:3]>>[*:0][c:1][c:2][*:3]',  #4
        
    '6-ring annelation 2': '''[c:4]1[c:3][ch:2][c:1]2[c:6]([c:5]1)[c:7][c:8][c:9]1[c:10][c:11][c:12][ch:13][c:14]12>>c1c[c:2]2[c:3][c:4][c:5][c:6]3[c:7][c:8][c:9]4[c:10][c:11][c:12][c:13]1[c:14]4[c:1]32''',
    'reverse_6-ring annelation 2': '''c1c[c:2]2[c:3][c:4][c:5][c:6]3[c:7][c:8][c:9]4[c:10][c:11][c:12][c:13]1[c:14]4[c:1]32>>[c:4]1[c:3][ch:2][c:1]2[c:6]([c:5]1)[c:7][c:8][c:9]1[c:10][c:11][c:12][ch:13][c:14]12''', #6

    '6-ring annelation 3': '[c:4]1[c:3][ch:2][c:1]2[c:6]([c:5]1)[c,C:7][c:9]1[c:10][c:11][c:12][ch:13][c:14]12>>c1c[c:2]2[c:3][c:4][c:5][c:6]3[*:7][c:9]4[c:10][c:11][c:12][c:13]1[c:14]4[c:1]32', #7
    'reverse_6-ring annelation 3': 'c1c[c:2]2[c:3][c:4][c:5][c:6]3[*:7][c:9]4[c:10][c:11][c:12][c:13]1[c:14]4[c:1]32>>[c:4]1[c:3][ch:2][c:1]2[c:6]([c:5]1)[*:7][c:9]1[c:10][c:11][c:12][ch:13][c:14]12', #8
    
    '5-ring annelation': '[ch:1]2[c:3][c:4][c:5][c:6]3[c:7][c:8][c:9][ch:2][c:10]23>>C1=C[c:1]2[c:3][c:4][c:5][c:6]3[c:7][c:8][c:9][c:2]1[c:10]23', #9
    'reverse_5-ring annelation': '[ch,CH]1=,:[ch,CH][c:1]2[c:3][c:4][c:5][c:6]3[c:7][c:8][c:9][c:2]1[c:10]23>>[ch:1]2[c:3][c:4][c:5][c:6]3[c:7][c:8][c:9][ch:2][c:10]23', #10
    
    'Ring contraction': '[c:0]1[ch:1][ch][c:3][c!r5:4][c!r5:5]1>>[c:0]1[CH2:1][c:3][c:4][c:5]1', #11
    'reverse_Ring contraction': '[CH2:0]1[c,C:1]-,=,:[c,C:2]-,=,:[c,C:3]-,=,:[c,C:4]1>>[ch:0]1c[c:1][c:2][c:3][c:4]1', #12
    
    'Linkage doublebond': '[ch&r6,CH1&r5:1]>>[*:1](-C=C-c1ccccc1)', #13
    'reverse_Linkage doublebond': '[c&r6,C&r5:1](-[C!R]=[C!R]-c1ccccc1)>>[ch&r6,CH1&r5:1]', #14
    'Linkage triplebond': '[ch&r6,CH1&r5:1]>>[*:1](-C#C-c1ccccc1)', #15
    'reverse_Linkage triplebond': '[c&r6,C&r5:1](-C#C-c1ccccc1)>>[ch&r6,CH1&r5:1]', #16
    
    # TTF formation
    'Fulvalene formation': '[r5:1][CH2r5:2][r5:3]>>[r5:1][C:2](=C1cccc1)[r5:3]', #17
    'reverse_Fulvalene formation': '[r5:1][C:2](=C1cccc1)[r5:3]>>[r5:1][CH2r5:2][r5:3]' #18

}

operations_typical_osc_design = deepcopy(operations_typical_osc_design_ch)

operations_typical_osc_design.update({

    # 5-ring: oxygen    
    'O 5-ring CH2 substitution': '[#6H2r5:1]>>[O:1]', #19
    'reverse_O 5-ring CH2 substitution': '[n,c:0]1[n,ch:1][o,O:2][n,c:3][n,c:4]1>>[*:0]1=[*:1][CH2:2][*:3]=[*:4]1', #20
    'Dioxole formation': '[CH,ch:0]1=[C,c:1][C,c:2]=[CH,ch:3][*:4]1>>[O:0]1[*:1]=,:[*:2][O:3][*:4]1', #21
    'reverse_Dioxole formation': '[S,s:0]1[r5:1]-,=,:[r5:2][S,s:3][*:4]1>>[CH:0]1=[C:1][C:2]=[CH:3][*:4]1',  #22
    
    # 5-ring: sulfur
    'S 5-ring CH2 substitution': '[#6H2r5:1]>>[S:1]', # 23
    'reverse_S 5-ring CH2 substitution': '[n,c:0]1[n,ch:1][s,S:2][n,c:3][n,c:4]1>>[*:0]1=[*:1][CH2:2][*:3]=[*:4]1', #24
    'Dithiole formation': '[CH,ch:0]1=[C,c:1][C,c:2]=[CH,ch:3][*:4]1>>[S:0]1[*:1]=,:[*:2][S:3][*:4]1', #25
    'reverse_Dithiole formation': '[O,o:0]1[r5:1]-,=,:[r5:2][O,o:3][*:4]1>>[CH:0]1=[C:1][C:2]=[CH:3][*:4]1', #26 
    
    # 5-ring: nitrogen    
    'N 5-ring CH2 substitution': '[#6H2r5:1]>>[NH:1]', #27
    'reverse_N 5-ring CH2 substitution': '[n,c:0]1[n,ch:1][NH:2][n,c:3][n,c:4]1>>[*:0]1=[*:1][CH2:2][*:3]=[*:4]1', #28
    'N 5-ring CH substitution': '[chr5,CH1r5:1]>>[n:1]', #29
    'reverse_N 5-ring CH substitution': '[nr5:0]>>[ch&r5:0]', #30

    # 6-ring nitrogen
    'N 6-ring substitution': '[ch&r6:0]>>[nr6:0]', #31
    'reverse_N 6-ring substitution': '[nr6:0]>>[ch&r6:0]', #32
    
    # Linkers are treated like sidegroups
    'Phenylamine linkage': '[ch&r6,CH1&r5:1]>>[*:1](-N-c1ccccc1)', #33
    'reverse_Phenylamine linkage': '[c&r6,C&r5:1](-[N!R]-c1ccccc1)>>[*:1]', #34
    
    # Add triphenyl
    'Triphenylamine linkage':'[r:1][NH!R:2][r:3]>>[r:1][N:2](-c1ccccc1)[r:3]', #35
    # reverse
    'reverse_Triphenylamine linkage': '[r:1][N!R:2](-c1ccccc1)[r:3]>>[r:1][NH:2][r:3]', #36
    
    # Building NTCDA or quinacridone
    '2-Pyrone formation': '[c:1]1[c:2][ch:3][ch:4][c:5][c:6]1>>[c:1]1[c:2][o:3][c:4](=O)[c:5][c:6]1', #37
    'reverse_2-Pyrone formation': '[o:1][c:2](=O)>>[ch:1][ch:2]', #38
    'Dianhydride formation': '[o:1][ch:2]>>[o:1][c:2](=O)', #39
    'reverse_Dianhydride formation': '[o:1][c:2](=O)>>[o:1][ch:2]', #40
    '4-pyron formation':'[ch:1]1[c:2][c:3][ch:4][c:5][c:6]1>>[o:1]1[c:2][c:3][C:4](=O)[c:5][c:6]1', #41
    'reverse_4-pyron formation':'[o:1][c:2][c:3][c:4](=O)>>[ch:1][c:2][c:3][ch:4]', #42
    'O-N exchange': '[o:1]>>[nH:1]', #43
    'reverse_O-N exchange': '[nH:1]>>[o:1]' #44

})

