""" These paths to executables are used by the calculation scripts in the scripts folder of this package.
    Please adjust them according to your needs.
"""

import os

# Settings for LRZ Cluster
if os.path.isdir("/dss/dssfs02/lwp-dss-0001/pr47fo/pr47fo-dss-0000/ga62yex2"):
    n_cpu = 12
    base_dir='/dss/dssfs02/lwp-dss-0001/pr47fo/pr47fo-dss-0000/ga62yex2/conda_py37/custom_packages/osc_discovery'        
    aims_species = os.path.join(base_dir, 'misc/species_defaults_c_schober')
    aims_binary = '/dss/dssfs02/lwp-dss-0001/pr47fo/pr47fo-dss-0000/ga62yex2/binaries_for_mpp2/bin/aims.200612.scalapack.mpi.x'
    mpiexe = 'mpirun' 
    if 'SLURM_JOB_PARTITION' in os.environ.keys():
        if 'mpp3' in os.environ['SLURM_JOB_PARTITION']:
            aims_binary = '/dss/dssfs02/lwp-dss-0001/pr47fo/pr47fo-dss-0000/ga62yex2/binaries_for_mpp2/bin/aims.200612.scalapack.mpi.x.mpp3'
        else:
            aims_binary = '/dss/dssfs02/lwp-dss-0001/pr47fo/pr47fo-dss-0000/ga62yex2/binaries_for_mpp2/bin/aims.200612.scalapack.mpi.x'
    run_aims = '{} -n {} {} > aims.out'.format(mpiexe, n_cpu, aims_binary)


# Settings for local system
elif os.path.isdir("/data/kunkel"):
    n_cpu = 12
    base_dir='/data/kunkel/git/orgel_base'
    aims_species = os.path.join(base_dir, 'misc/species_defaults_c_schober')
    mpiexe = 'mpirun' 
    aims_binary = '/data/muschielok/code/bin/aims.190101.scalapack.mpi.x'
    run_aims = '{} -n {} {} > aims.out'.format(mpiexe, n_cpu, aims_binary)

else:
    print("System couldn't be identified, please provide computational settings for your system in aims_settings.py")
