''' AML discovery in the chemical space

Code associated with the article: "Active Discovery of Organic Semiconductors"

The execution of these workflows is possible by scripts/active_learner_run.py

Author: Christian Kunkel (christian.kunkel@tum.de)
'''