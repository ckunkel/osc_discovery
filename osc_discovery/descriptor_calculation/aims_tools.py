import os, shutil
import numpy as np
import pandas as pd
from collections import OrderedDict

from ase.io import read
from ase.constraints import StrainFilter
from ase.calculators.aims import Aims
from ase.optimize import BFGS

from aimsutils import aims_calc_params
from osc_discovery.aims_settings import aims_binary, aims_species, n_cpu, mpiexe


""" Helper functions to extract information from FHI-aims output/config files """


def prepare_aims_fodft(ncpu_in=n_cpu, xc_in="blyp",
                 	species_in="light.ext", mpiexec=''):
    """ Prepare the aims_params dict for the ASE aims calculator. """
    
    if mpiexec=='':
        mpiexec=mpiexe
 
    aims_params = aims_calc_params(binary_path=aims_binary,
                                   species_path=aims_species,
                                   xc=xc_in,
                                   species=species_in,
                                   mpiexe=mpiexec,
                                   numcpu=ncpu_in,
                                   default="organic_fodft")

    #aims_params["default_initial_moment"]="0"
    aims_params["relativistic"] = "atomic_zora scalar"
    aims_params["packed_matrix_format"] = "index" 
    aims_params['default_initial_moment']='0'

    return aims_params


def run_aims_on_mol(mol, aims_add_params={}, xc_in="b3lyp", label='.', species_in="light.ext", ncpu_in=None, mpiexec=''):  
    ''' Execute a simple FHI-aims calculation '''
    
    if ncpu_in==None:
        ncpu_in=n_cpu
        
    if mpiexec=='':
        mpiexec=mpiexe
        
    aims_params=prepare_aims_fodft(xc_in=xc_in, species_in=species_in, ncpu_in=ncpu_in, mpiexec=mpiexec)
    aims_params.update(aims_add_params)

    aims_calc = Aims(label=label,**aims_params) 

    mol.set_calculator(aims_calc)

    energy = mol.get_total_energy()
    
    return energy, mol


def check_geometry_convergence(PATH_TO_FILE):
    ''' Check if the present geometry relaxation has converged '''
    success=False
    with open (PATH_TO_FILE,'r') as aims_out:
        for line in aims_out:
            if 'Present geometry is converged' in line:
                success=True
    return success


def check_aimsrun_finished(PATH_TO_FILE):
    ''' Check if the present calculation was finished successfully '''
    success=False
    with open (PATH_TO_FILE,'r') as aims_out:
        for line in aims_out:
            if 'Have a nice day' in line:
                success=True
        return success
    

def extract_eigenvalue_spectrum(output_folder, outfile="aims.out"):
    """ Extracts the eigenvalue spectrum from an aims.out file and stores 
        it in a pandas dataframe
    """
    n_spin_channels = 2 # Starting with the assumption of a spin-polarised calculation

    #1) get the number of eigenvalue spectra written to the file
    out = []
    with open (os.path.join(output_folder,outfile) ,"r") as output:
        num_occurences=0
        for line in output:
            if 'Number of spin channels' in line:
                n_spin_channels = int(line.split(':')[1])
            if "Spin treatment: No spin polarisation." in line:
                n_spin_channels = 1 # Only one spin channel
            if "Writing Kohn-Sham eigenvalues." in line:
                num_occurences+=1
            out.append(line)

    #2) determine, in which line the spin_up spectrum starts
    if True:
        num_found=0
        line_start_spinup=0
        o=-1
        for line in out:
            o=o+1
            if "Writing Kohn-Sham eigenvalues." in line:
                num_found+=1
                if num_found==num_occurences: #This is the last spectrum, the one we want
                    line_start_spinup=o+2*n_spin_channels # Aims prints 2 extra lines in spin polarised case

    #3) extract spin_up spectrum
    if True:
        o=-1
        lines_spectrum_spinup=[]
        for line in out:
            o+=1
            if o == line_start_spinup:
                lines_spectrum_spinup.append(line) #First line is the column title row
                line=out[o+1]
                while "." in line:
                    o=o+1
                    line=out[o]
                    lines_spectrum_spinup.append(line)
                line_start_spindown=o+3
                break

    # For a non-spinpolarised calculation we are done
    df_spectrum_spinup=_parse_eigenvalue_spectrum(lines_spectrum_spinup)
    if n_spin_channels == 1:
        return df_spectrum_spinup

    #4) extract spin_down spectrum
    if True:
        o=-1
        lines_spectrum_spindown=[]
        for line in out:
            o+=1
            if o == line_start_spindown:
                lines_spectrum_spindown.append(line) #First line is the column title row
                line=out[o+1]
                while "." in line:
                    o=o+1
                    line=out[o]
                    lines_spectrum_spindown.append(line)
                break

    df_spectrum_spindown = _parse_eigenvalue_spectrum(lines_spectrum_spindown)
    return [df_spectrum_spinup, df_spectrum_spindown]


def _parse_eigenvalue_spectrum(lines_spectrum):
    """ Function takes the extracted lines and parses them further
    Results are then saved in a pandas dataframe
    """
    #extract column headers and create dataframe
    columns=lines_spectrum[0].split("\n")[0].split("   ")
    columns=[x.strip() for x in columns]
    columns=[x.replace(" ","_") for x in columns]
    df_spectrum=pd.DataFrame(columns=columns)
    
    #Parse remaining spectrum
    for line in lines_spectrum[1:]:
        line_split=line.split()
        if not len(line_split)==0:
            #print(len(line_split))
            df_add=pd.DataFrame(columns=columns, data=[line_split[0:4]])
            df_spectrum=pd.concat([df_spectrum, df_add],ignore_index=True)

    return df_spectrum


def get_homo_lumo_energy(calc_folder, outfile='aims.out'):
    """ Finds HOMO and LUMO from the eigenvalue spectrum """
    
    df_spectrum = extract_eigenvalue_spectrum(calc_folder, outfile=outfile)
    if not isinstance(df_spectrum, list):
        df_spectrum_spinup = df_spectrum
        df_spectrum_spindown = df_spectrum
        n_spin_channels=1
    else:
        df_spectrum_spinup = df_spectrum[0]
        df_spectrum_spindown = df_spectrum[1]	
        n_spin_channels=2
        
    lumo_spinup=0.0
    lumo_spindown=0.0
    state_number_homo_spinup=0
    state_number_homo_spindown=0
    state_number_lumo_spinup=0
    state_number_lumo_spindown=0
    channel_homo=0
    channel_lumo=0
    
    for i,row in df_spectrum_spinup.iterrows():
        if float(row['Occupation']) > 0.1:
            homo_spinup=float(row['Eigenvalue_[eV]'])
            state_number_homo_spinup=row['State']
        if float(row['Occupation']) < 0.1:
            lumo_spinup=float(row['Eigenvalue_[eV]'])
            state_number_lumo_spinup=row['State']
            break
            
    for i,row in df_spectrum_spindown.iterrows():
        if float(row['Occupation']) > 0.1:
            homo_spindown=float(row['Eigenvalue_[eV]'])
            state_number_homo_spindown=row['State']
        if float(row['Occupation']) < 0.1:
            lumo_spindown=float(row['Eigenvalue_[eV]'])
            state_number_lumo_spindown=row['State']
            break
            
    # Now find really the highest/lowest from both spinchannels
    if homo_spinup >= homo_spindown:
        homo_en=homo_spinup
        channel_homo='up'
        state_number_homo=state_number_homo_spinup
    else:
        homo_en=homo_spindown
        channel_homo='down'
        state_number_homo=state_number_homo_spindown

    if lumo_spinup <= lumo_spindown:
        lumo_en=lumo_spinup
        channel_lumo='up'
        state_number_lumo=state_number_lumo_spinup
    else:
        lumo_en=lumo_spindown
        channel_lumo='down'
        state_number_lumo=state_number_lumo_spindown

    return {'HOMO_energy': homo_en, 
            'LUMO_energy': lumo_en, 
            'HOMO_channel': channel_homo, 
            'LUMO_channel': channel_lumo, 
            'State_number_HOMO': state_number_homo, 
            'State_number_LUMO': state_number_lumo,
            "n_spin_channels": n_spin_channels}


def read_total_energy(outfile_aims):
    """ Read total energy from an aims out file
    Tested for cluster calculation 
    """
    with open(outfile_aims) as out:
        for line in out.readlines():
            if '| Total energy of the DFT / Hartree-Fock s.c.f. calculation' in line:
                total_en=float(line.split(':')[-1].split('eV')[0])
    return total_en