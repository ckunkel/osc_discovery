import os
import shutil
from ase import Atoms
from ase.io import read, write
from copy import deepcopy
from rdkit import Chem
import multiprocessing as mp

from osc_discovery.descriptor_calculation.conformers import get_conformers_rdkit as get_conformers
from osc_discovery.cheminformatics.cheminformatics_misc import ase2xyz, ase2rdkit
from osc_discovery.cheminformatics.cheminformatics_symmetry import Symmetry

''' Tools to calculate molecular descriptors using a computationally cheap xTB methods '''

def _run_single_mult(atoms):
    """ helper for the parallel processing of conformers
        No logging for parallel run."""
    res = run_single(atoms, method=atoms.info['method'], 
                     idx=atoms.info['idx'],
                     optlevel=atoms.info['optlevel'],
                     mpiexe=atoms.info['mpiexe'],
                     n_procs=atoms.info['n_procs'], log=True)
    return res


def find_lowest_energy_conformer_xtb(atoms_ase_list, method='GFN1', 
                                     optlevel='tight', return_all=False, 
                                     multi_process=0, n_proc_each=2, 
                                     mpiexe='', write_to_folder=''):
    ''' Find the lowest-energy conformer after xTB optimization from a list of ase atoms '''
    
    energies=[]
    cwd=os.getcwd()
    
    print('Number of conformers to process: {}'.format(len(atoms_ase_list)))
    
    if write_to_folder!='':
        write( os.path.join(write_to_folder,'full_conformer_screening_xtb_intial.xyz'), atoms_ase_list )
    
    try:
        shutil.rmtree('run_*')
    except: pass

    if multi_process==0:
        for i,atoms in enumerate(atoms_ase_list):
            atoms_ase_list[i] = run_single(atoms, method=method, idx=i, optlevel=optlevel)
            energies.append(atoms_ase_list[i].info['total_energy_eV'])
    else:      
        for i, atoms in enumerate(atoms_ase_list):
            atoms_ase_list[i].info['idx']=i
            atoms_ase_list[i].info['method']=method
            atoms_ase_list[i].info['optlevel']=optlevel
            atoms_ase_list[i].info['mpiexe']=mpiexe
            atoms_ase_list[i].info['n_procs']=n_proc_each #int(multi_process)

        pool = mp.Pool(processes=multi_process)
        atoms_ase_list = pool.map(_run_single_mult, atoms_ase_list)
        energies=[x.info['total_energy_eV'] for x in atoms_ase_list]
    
    os.chdir(cwd)
    os.system('rm -r run_*')

    if write_to_folder!='':
        write( os.path.join(write_to_folder,'full_conformer_screening_xtb_relaxed.xyz'), atoms_ase_list )

    if return_all: return atoms_ase_list, energies
    return atoms_ase_list[energies.index(min(energies))], energies[energies.index(min(energies))]



def run_opf_bfgs(ase_atoms, command):
    ''' Fallback for bfgs optimization, if ANCOPT didn't work '''

    ase_atoms.write('scratch.coord', format='turbomole') 
    with open('scratch.coord', 'r') as out:
        coords=out.read()
    coords=coords.replace('$end\n', '')
    with open('opt.inp', 'w') as out:
        out.write(coords+'''$opt
    engine=lbfgs
$end''')

    os.system( command.replace('scratch.xyz', 'opt.inp') )
    print(command.replace('scratch.xyz', 'opt.inp'), os.getcwd())
    ase_atoms=read('xtbopt.inp')
    os.system('xtb --hess --gfnff xtbopt.inp > freq.out')

    return ase_atoms





def run_single(ase_atoms, method='GFN1', idx=0, optlevel='vtight', chg=0, mpiexe='', n_procs=0, log=True):
    ''' Optimize a single molecular conformer '''
    
    cmd = ''
    if mpiexe!='': cmd += mpiexe+' --np 1 '
   
    cmd += 'xtb scratch.xyz'
    
    if n_procs>=1: cmd+=' -P {}'.format(n_procs)
    
    if 'ff' in method: cmd +=' --gfnff '
    else: cmd +=' --gfn {} '.format(method.split('GFN')[1])

    ase_atoms=deepcopy(ase_atoms)
    
    if optlevel!=None: cmd+='--opt {} '.format(optlevel)
    if chg!=0: cmd+='--uhf 1 '
        
    os.mkdir('run_{}'.format(idx))
    os.chdir('run_{}'.format(idx))

    ase_atoms.write('scratch.xyz')
    cmd+='--chrg {} --acc 0.2'.format(chg) #--acc 0.2 for production, default for testspace 

    if log: cmd+=' > E0G0.log'
    print(cmd, os.environ['OMP_NUM_THREADS'])
    os.system(cmd)
    
    if optlevel!=None: 
        # fallback option if ANCOPT failed
        if not os.path.isfile('xtbopt.xyz'): ase_atoms = run_opf_bfgs(ase_atoms, cmd)
        else: ase_atoms=read('xtbopt.xyz')
    homo_en, lumo_en, tot_en = read_homo_lumo_energy_xtb('E0G0.log')

    print(idx, tot_en)
    os.chdir('..')
    shutil.rmtree('run_{}'.format(idx))

    ase_atoms.info['total_energy_eV'] = tot_en
    ase_atoms.info['HOMO_en_eV'] = homo_en
    ase_atoms.info['LUMO_en_eV'] = lumo_en

    return ase_atoms



def read_homo_lumo_energy_xtb(logfile_name):
    ''' read HOMO-, LUMO-, and totalenergy from logfile, returns values in eV '''
    homo_en=0.; lumo_en=0.; tot_en=0.
    
    with open(logfile_name) as out:
        for line in out.readlines():
            if '(HOMO)' in line:
                homo_en=float(line.split()[-2])
            if '(LUMO)' in line:
                lumo_en=float(line.split()[-2]) 
            if 'TOTAL ENERGY' in line:
                tot_en=float(line.split('TOTAL ENERGY')[1].split('Eh')[0])
                                    
    return homo_en, lumo_en, tot_en*27.2113845



def calculate_reorganization_energy_xtb_cmd(method, atoms, idx, optlevel='vtight', mode='hole', n_procs=2):
    ''' Calculate a singe reorganization energy '''
    
    if method=='XTB1' or method=='GFN1': method_internal='GFN1'
    else: method_internal='GFN2'
        
    if os.path.isdir('run_{}'.format(idx)):
        shutil.rmtree('run_{}'.format(idx))
    os.mkdir('run_{}'.format(idx))
    os.chdir('run_{}'.format(idx))

    if mode=='electron': chg=-1
    else: chg=1
        
    atoms_E0G0 = run_single(deepcopy(atoms), method=method_internal, idx=idx, optlevel=optlevel, chg=0, n_procs=n_procs)
    atoms_EpG0 = run_single(deepcopy(atoms_E0G0), method=method_internal, idx=idx, optlevel=None, chg=chg, n_procs=n_procs)
    atoms_EpGp = run_single(deepcopy(atoms_E0G0), method=method_internal, idx=idx, optlevel=optlevel, chg=chg, n_procs=n_procs)
    atoms_E0Gp = run_single(deepcopy(atoms_EpGp), method=method_internal, idx=idx, optlevel=None, chg=0, n_procs=n_procs)
    
    E0G0 = atoms_E0G0.info['total_energy_eV']
    EpG0 = atoms_EpG0.info['total_energy_eV']
    EpGp = atoms_EpGp.info['total_energy_eV']
    E0Gp = atoms_E0Gp.info['total_energy_eV']

    e_lambda = ((EpG0 + E0Gp) - (EpGp + E0G0))*1000 #meV
    
    os.chdir('..')
    os.system('rm -r run_{}'.format(idx))
    
    print("Estimated reorganization energy (meV, {}): {} for {}".format(mode, e_lambda, idx))

    return [ e_lambda, idx, [atoms_E0G0, atoms_EpGp, atoms_EpG0, atoms_E0Gp] ]



def calculate_single_molecule_properties_xtb(smi, multiproc_conformers=0, n_proc_each=2, method='GFN1', max_conformers=-1, scratchdir='', mpiexe=''):
    ''' Calculate all properties for a single molecule '''
   
    # Determine symmetry
    symmetry=Symmetry()(Chem.MolFromSmiles(smi))
    
    # Get conformers from rdkit
    if multiproc_conformers<1: n_cpu=1
    else: n_cpu = multiproc_conformers
    if max_conformers==-1: ase_atoms_conformers = get_conformers(smi, n_cpu=n_cpu)
    else: ase_atoms_conformers = get_conformers(smi, max_conformers=max_conformers, n_cpu=n_cpu)

    cwd = os.getcwd()
    if scratchdir != '':
        if os.path.isdir(scratchdir): shutil.rmtree(scratchdir)
        os.mkdir(scratchdir)
        os.chdir(scratchdir)

    # Get most stable conformer from xTB ranking at tight optimization level
    atoms_neut, energy = find_lowest_energy_conformer_xtb(ase_atoms_conformers,
                                                          optlevel='vtight', # normal was used for testspace
                                                          method=method, return_all=False, 
                                                          multi_process=multiproc_conformers, n_proc_each=n_proc_each,
                                                          mpiexe=mpiexe, write_to_folder=cwd)

    # Method and optimization level used for lambda calculation
    optlevel = 'vtight'
    method = 'GFN1'
    
    # Get the hole-reorganization energy
    e_lambda, idx, atoms_list_h = calculate_reorganization_energy_xtb_cmd(method, atoms_neut, 0,
                                                                          optlevel=optlevel, n_procs=n_proc_each)
        
    # Get the electron-reorganization energy
    e_lambda_e, idx, atoms_list_e = calculate_reorganization_energy_xtb_cmd(method, atoms_neut, 0, optlevel=optlevel, 
                                                                            mode='electron', n_procs=n_proc_each)
    
    atoms_res = atoms_list_h[0]
    atoms_res.info['e_lambda_h'] = e_lambda
    atoms_res.info['e_lambda_e'] = e_lambda_e
    atoms_res.info['symmetry'] = symmetry
    
    if scratchdir != '':
        os.chdir(cwd)
        shutil.rmtree(scratchdir)

    return atoms_res
