import os, time, json
from copy import deepcopy

import ase
from ase.io import read,write
from ase.io.aims import read_aims_output
from ase.optimize import BFGS
from ase.constraints import FixAtoms
from ase.calculators.aims import Aims

from aimsutils import aims_calc_params
from osc_discovery.descriptor_calculation.aims_tools import check_geometry_convergence
from osc_discovery.descriptor_calculation.aims_tools import run_aims_on_mol, get_homo_lumo_energy

""" Contains a workflow to calculate reorganization energies using FHI-aims. """

class LambdaGasPhase:
    def __init__(self, 
                 aims_cmd, 
                 aims_species, 
                 xc, 
                 mol_ase, 
                 n_cpu, 
                 occupation_type="gaussian 0.01", 
                 mol_ase_charged=False, 
                 preoptimize=True, 
                 optimize=True, 
                 preoptimize_charged=False,
                 species_name_preoptimize='light', 
                 species_name_optimize='tight', 
                 species_name_preoptimize_charged='light',
                 charge=1, 
                 f_max='0.01', 
                 additional_parameters={}, 
                 mpiexe='mpirun', 
                 use_restarts=True, 
                 optimizer_type='internal_aims_bfgs'):
        """
        Calculates reorganization energies using FHI-AIMS and ASE (from python)

        aims_cmd:
            The command string to be used for running FHIaims. Path and output
            pipe, e.g.
            /data/schober/bin/aims.151107.scalapack.mpi.x > aims.out
        aims_species:
            Path to FHIaims species directory, e.g.,
            .../species_defaults
        xc:
            The XC functional to be used for ALL QM calculations.
        mol_ase: ase.Atoms
            The ase.Atoms object for the central QM-molecule.
        n_cpu:
            The number of CPU to be used for the DFT calculations.
        mol_ase_charged: ase.Atoms, optional
            Starting geometry for charged molecule geometry. If not supplied, optimized neutral geometry is used
        preoptimize: Boolean Flag, optional
            Preoptimize neutral geo at the lighter level, or proceed to tight optimization directly?
        optimize: Boolean Flag, optional
            Whether to optimize at all. If you supply reasonable geometries for neutral and charged state, 
            it might not be necessary and single point calculations might do.
        preoptimize_charged: Boolean flag, optional
            Preoptimize charged geo at the lighter level, or proceed to tight optimization directly?
        species_name_preoptimize: str, optional
            Basis set name for preoptimization, as contained in aims_species folder, default: light
        species_name_optimize: str, optional
            Basis set name for optimization, as contained in aims_species folder, default: tight
        charge: int, optional
            Charge of the charged species in the calculation
        f_max: float, optional
            force criterion for BFGS geometry optimization
        additional_params: dict, optional
            Additional parameters for aims calculation
        mpiexe: str, optional
            Mpicommand to invoke aims
        use_restarts: 
            If an ASE calculator is invoked, are restart files used?
        optimizer_type: 
            Internal (aims BFGS) or external (ASE) optimizer
        """
        self.mpiexe = mpiexe
        self.aims_cmd = aims_cmd
        self.aims_species = aims_species
        self.xc = xc
        self.n_cpu = n_cpu
        self.occupation_type = occupation_type
        self.preoptimize = preoptimize
        self.optimize = optimize
        self.preoptimize_charged = preoptimize_charged
        self.species_name_preoptimize = species_name_preoptimize
        self.species_name_optimize = species_name_optimize
        self.species_name_preoptimize_charged = species_name_preoptimize_charged
        self.charge = charge
        self.f_max = f_max 
        self.additional_parameters = additional_parameters
        self.use_restarts = use_restarts
        self.optimizer_type = optimizer_type
        
        # Unfortunate, but roughly from ase v3.20 on label was renamed to 
        # directory. So we need to handle it
        self.label_field = 'label'
        ase_version = [int(x) for x in ase.__version__.split('.')]
        if ase_version[0]>=3 and ase_version[1]>=20: 
            self.label_field = 'directory'
            
 
        # write settings:
        with open('setttings_lambda_calc.txt', 'w') as file:
            file.write(json.dumps(self.__dict__))

        self.mol_ase = mol_ase
        print(mol_ase)
        self.mol_ase_charged = mol_ase_charged


    def prepare_aims(self, charge, species_in="tight", force=False, optimize_internal=False):
        """
        Prepare the aims_params dict for the ASE aims calculator.
        """
        aims_params = aims_calc_params(binary_path=self.aims_cmd,
                                       species_path=self.aims_species,
                                       xc=self.xc, species=species_in,
                                       mpiexe=self.mpiexe, numcpu=self.n_cpu)
        aims_params["relativistic"] = "atomic_zora scalar 1e-12"
        #aims_params["packed_matrix_format"] = "index"

        aims_params["default_initial_moment"] = charge
        aims_params["use_calculate_fock_matrix_old"] = '.false.'
       
        # since we require converged forces, these should already be well converged
        aims_params["sc_accuracy_rho"]='1e-5'
        aims_params["sc_accuracy_eev"]='1e-3'

        aims_params['aggregated_energy_tolerance']='10e-3'

        aims_params["charge"] = charge
        
        if self.use_restarts:
            aims_params["restart_write_only"] = "restart.f"
            aims_params["restart_read_only"] = "restart.f"

        aims_params["charge_mix_param"] = "0.1"
        aims_params["occupation_type"] = self.occupation_type #Added, can be used, but so far not necessary
        
        aims_params["output hirshfeld"] = " "

        if force:
            aims_params["sc_accuracy_forces"] = "1E-5"

        if optimize_internal:
            aims_params["relax_geometry"] = "bfgs {}".format(self.f_max)

        aims_params.update(self.additional_parameters)

        return aims_params

    def run_task(self):
        time_start=time.time()
        self.energies = {}
        cwd = os.getcwd()

        if not self.optimize:
            print("Attention: Running only single point calculations on the supplied geometry(ies)")        


        # ITERATION 1, light, neutral
        if self.preoptimize and self.optimize:
                aims_params = self.prepare_aims(species_in=self.species_name_preoptimize, charge=0,force=True, 
                                                optimize_internal=self.optimizer_type=='internal_aims_bfgs')
                aims_params[self.label_field] = "qm_light"
                QM = Aims(**aims_params)
                write('before-light.xyz', self.mol_ase)
                self.mol_ase.set_calculator(QM)
                if not 'relax_geometry' in aims_params.keys():
                    light_neut_opt = BFGS(self.mol_ase, trajectory='light_neut.traj')
                    light_neut_opt.run(fmax=self.f_max)
                else:
                    print(aims_params)
                    print(self.mol_ase)
                    self.mol_ase.get_potential_energy()
                    if not check_geometry_convergence(os.path.join(cwd,"qm_light",'aims.out')):
                        print('Geometry convergence error qm_light, retrying at qm_tight')
                        self.mol_ase = read(os.path.join(cwd,"qm_light",'geometry.in.next_step', format='aims'))
                    else:
                        self.mol_ase = read_aims_output(os.path.join(cwd,"qm_light",'aims.out'))
                write('after-light.xyz', self.mol_ase)


        # ITERATION 2, tight, neutral
        aims_params = self.prepare_aims( species_in=self.species_name_optimize, charge=0, force=True,
                                         optimize_internal=(self.optimizer_type=='internal_aims_bfgs' and \
                                                            self.optimize) )
        aims_params[self.label_field] = "qm_tight"
        QM = Aims(**aims_params)
        self.mol_ase.set_calculator(QM)
        if self.optimize and not 'relax_geometry' in aims_params.keys():
                tight_neut_opt = BFGS(self.mol_ase, trajectory='tight_neut.traj')
                tight_neut_opt.run(fmax=self.f_max)
        self.energies["E0G0"] = self.mol_ase.get_total_energy()
        
        if 'relax_geometry' in aims_params.keys():
            if not check_geometry_convergence(os.path.join(cwd,"qm_tight",'aims.out')):
                print('Geometry convergence error qm_tight')
                sys.exit (1)
            # need to read the total energy also so that it correctly appears in after-tight.xyz
            self.mol_ase = read_aims_output(os.path.join(cwd,"qm_tight",'aims.out'))

        print("E0G0: {}".format(self.energies["E0G0"]))
        self.opt_neutral = deepcopy(self.mol_ase)
        write('after-tight.xyz', self.mol_ase)


        # ITERATION 3, tight, charged
        if not self.mol_ase_charged: #If no charged starting geometry supplied, nothing needs to happen
               print("Using neutral starting geometry for charged optimization")
        else:
               print("Using the supplied starting geometry for charged optimization")
               self.mol_ase=deepcopy(self.mol_ase_charged)
        aims_params = self.prepare_aims(species_in=self.species_name_optimize, charge=self.charge, force=True,
                                         optimize_internal=(self.optimizer_type=='internal_aims_bfgs' and \
                                                            self.optimize) )
        aims_params[self.label_field] = "qm_tight_charged"
        aims_params["sc_iter_limit"] = 500 
        QM = Aims(**aims_params)
        self.mol_ase.set_calculator(QM)
        if self.optimize and not 'relax_geometry' in aims_params.keys():
            tight_char_opt = BFGS(self.mol_ase, trajectory='tight_char.traj')
            tight_char_opt.run(fmax=self.f_max)
        self.energies["E+G+"] = self.mol_ase.get_total_energy()
        
        if 'relax_geometry' in aims_params.keys():
            if not check_geometry_convergence(os.path.join(cwd,"qm_tight_charged",'aims.out')):
                print('Geometry convergence error qm_tight_charged')
                sys.exit (1)
            self.mol_ase = read_aims_output(os.path.join(cwd,"qm_tight_charged",'aims.out'))

        print("E+G+: {}".format(self.energies["E+G+"]))
        self.opt_charged = deepcopy(self.mol_ase)
        write('after-tight-charged.xyz', self.mol_ase)


        # SINGLE POINT 1, E+G0
        aims_params = self.prepare_aims(species_in=self.species_name_optimize, charge=self.charge, force=True)
        aims_params[self.label_field] = "qm_EpG0"
        aims_params["sc_iter_limit"] = 500 #We reduce max scf a bit, to only slightly relax the geometry first
        aims_params["output cube spin_density"] = ""
        QM_EpG0 = Aims(**aims_params)
        self.opt_neutral.set_calculator(QM_EpG0)
        self.energies['E+G0'] = self.opt_neutral.get_total_energy()
        write('after-EpG0.xyz', self.opt_neutral)
        print("E+G0: {}".format(self.energies["E+G0"]))


        # SINGLE POINT 2, E0G+
        aims_params = self.prepare_aims(species_in=self.species_name_optimize, charge=0, force=True)
        aims_params[self.label_field] = "qm_E0Gp"
        aims_params["sc_iter_limit"] = 500 #We reduce max scf a bit, to only slightly relax the geometry first
        QM_E0Gp = Aims(**aims_params)
        self.opt_charged.set_calculator(QM_E0Gp)
        self.energies['E0G+'] = self.opt_charged.get_total_energy()
        write('after-E0Gp.xyz', self.opt_charged)
        print("E0G+: {}".format(self.energies["E0G+"]))


        # Final lambda output
        self.e_lambda = ((self.energies["E+G0"] + self.energies["E0G+"]) -
                         (self.energies["E+G+"] + self.energies["E0G0"]))*1000
        print('Calculated lambda: {} meV'.format(self.e_lambda))
        with open("lambda.txt", "w") as f:
            f.write(str(self.e_lambda))


        with open('total_runtime.txt','w') as f:
            f.write(str(time.time()-time_start)+' s')

        return self.e_lambda
