import numpy as np
from copy import deepcopy

threshold=-0.2
    
def get_F(y, ideal_points=[0.0,-5.1], weights=[1.0,0.7],
                           std=[], kappa=1., return_array=False):
    
    ''' Compute utility function F and acquisition function Facq = F + k*\sigma '''

    y[0]/=1000. # lambda to eV
    
    utility_hole = -np.sqrt(( (y[0]-ideal_points[0]) * 1.0)**2 + ( (y[1]-ideal_points[1]) * 0.7)**2) 
    
    if len(std)>0:
        
        # for compatiblity with order in AL 
        std[0]/=1000.
        var_hole = np.sum( utility_hole**(-2) * np.array([weights[0], weights[1]])**4 * \
                          (np.array([ y[0], y[1] ]) - np.array( [ideal_points[0],ideal_points[1]] ))**2 * \
                           np.array( [std[0],std[1]] )**2 )
        
        std_hole = np.sqrt(var_hole)
        utility_hole += kappa*std_hole

        if return_array:
            return [ utility_hole, std_hole ]
    
    if return_array:
        return [ utility_hole, 0 ]
    
    return utility_hole

           

def linear_correct_to_b3lyp(propname, val):
    
    ''' Correct xTB-GFN1 descriptor-values to B3LYP using linear correlation '''    
    
    if propname=="XTB1_lamda_h": val = 1.45 * val + 28
    elif propname=="ehomo_gfn1_b3lyp": val = 0.92 * val + 4.43
    
    return val
