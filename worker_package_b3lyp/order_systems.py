import os
from rdkit import Chem
from pathlib import Path
import numpy as np


def dont_order(systems_to_calculate, folder_systems):
    return systems_to_calculate


def order_by_datetime(systems_to_calculate, folder_systems):
    ''' older files are run first '''
    paths = [str(x).split('/')[-1] for x in sorted(Path(folder_systems).iterdir(), key=os.path.getmtime)]
    paths = [x for x in paths if x in systems_to_calculate]
    return paths


def order_by_n_el(systems_to_calculate, folder_systems):

    n_els=[]
    for filex in systems_to_calculate:
        filepath = os.path.join(folder_systems, filex)
        smi = open(filepath, 'r').read()
        mol = Chem.AddHs(Chem.MolFromSmiles(smi))
        n_els.append(sum([at.GetAtomicNum() for at in mol.GetAtoms()]))
    idx_systems=np.flip(np.argsort(n_els))
    print(np.array(n_els)[idx_systems])
    return list(np.array(systems_to_calculate)[idx_systems])

