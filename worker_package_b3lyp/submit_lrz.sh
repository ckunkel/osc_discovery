#!/bin/sh
#SBATCH -J w_orgel_sc
#SBATCH --get-user-env 
#SBATCH --mail-type=end 
#SBATCH --mail-user=christian.kunkel@tum.de 
#SBATCH --export=NONE 
#SBATCH --time=72:00:00  
#SBATCH --ntasks-per-node=28

# for mpp3
##SBATCH --nodes=4
##SBATCH --clusters=mpp3

# for tiny queue (dft)
#SBATCH --nodes=4
#SBATCH --clusters=cm2_tiny

# for serial queue (gfn1)
##SBATCH --nodes=1
##SBATCH --cpus-per-task=1
##SBATCH --clusters=serial
##SBATCH --partition=serial_long

# for std queue (dft)
##SBATCH --nodes=20
##SBATCH --clusters=cm2
##SBATCH --partition=cm2_std

# for large queue
##SBATCH --clusters=cm2
##SBATCH --partition=cm2_large
##SBATCH --qos=cm2_large
##SBATCH --nodes=25

. /etc/profile
. /etc/profile.d/modules.sh

# Important for xtb, else it shares only one cpu
source /dss/dsshome1/lxc09/ga62yex2/.bashrc
export LAUNCHDIR=$PWD

# for xtb
ulimit -s unlimited
export KMP_AFFINITY=none
export OMP_NUM_THREADS=2

# For the workflow
export n_nodes=$SLURM_NNODES
export n_mpi_ranks_per_node=1
export n_nodes_per_worker=1
#export XTB_ONLY=1

export SCRATCH=$SCRATCH/$SLURM_JOBID
mkdir -p $SCRATCH
python run_workers.py worker_b3lyp.py molecules_to_calculate
rm -r $SCRATCH
