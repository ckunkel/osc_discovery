import os, sys, argparse
from rdkit import Chem
from osc_discovery.descriptor_calculation.xtb_tools import calculate_single_molecule_properties_xtb
from osc_discovery.acquisition_function import linear_correct_to_b3lyp

''' Computes descriptor values on a .smi file with the cheap xTB approximation '''

if __name__ == '__main__':

    system_to_calculate=sys.argv[1]
    with open(system_to_calculate) as out:
        smi=out.readlines()[0] 

    # if the result is not to be written to the external results file
    if len(sys.argv)==2: write=True 
    else: write=False; n_procs=int(sys.argv[2])

    scratchdir=''
    if 'SCRATCH' in os.environ.keys(): #linux cluster
        scratchdir = os.path.join( os.environ['SCRATCH'], system_to_calculate )
   
    print(smi) 
    
    # Run GFN1 lambda calcualtion
    method='GFN1'
    atoms_res = calculate_single_molecule_properties_xtb(smi, 
                                                         multiproc_conformers=n_procs,
                                                         scratchdir=scratchdir)
    atoms_res.write('atoms_neut_xtb_res.xyz')
    homo_en = linear_correct_to_b3lyp('ehomo_gfn1_b3lyp',  atoms_res.info['HOMO_en_eV'])
    e_lambda_h = linear_correct_to_b3lyp('XTB1_lamda_h',  atoms_res.info['e_lambda_h'])

    print(homo_en, e_lambda_h, homo_en)
    if write: os.system('echo "{} {} {}" >> ../../results_calculations.txt'.format(system_to_calculate, smi, e_lambda_h, homo_en))

