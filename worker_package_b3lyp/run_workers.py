from joblib import Parallel, delayed
import os, socket, time, sys
from hostlist import expand_hostlist

""" This script is part of the workflow and starts the single workers
    It is usually called from a submit script """

worker_script = sys.argv[1]
folder_run = sys.argv[2]
n_jobs = int(int(os.environ['n_nodes']) / float(os.environ['n_nodes_per_worker']))
print('N_jobs:', n_jobs)

def run_worker(start, worker_script, folder_run, hostname):
    time.sleep(float(start*2))
    cmd='python {} {} {}'.format(worker_script, folder_run, hostname)
    os.system(cmd)
    return None

if __name__ == '__main__':
    
    # On lrz linux cluster
    # So far this works for one node per worker, but could be easily extended
    if 'SLURM_NODELIST' in os.environ:
        node_list = expand_hostlist(os.environ['SLURM_NODELIST'])
    else:
        node_list=['0'] * int(os.environ['n_nodes'])

    #overcommitting on noded
    if len(node_list)<n_jobs and n_jobs%len(node_list)==0:
        print('Overcommitting by a factor of {}'.format(int(n_jobs/len(node_list))))
        node_list = node_list * int(n_jobs/len(node_list))

    # run
    results=[]
    with Parallel(n_jobs=n_jobs, verbose=0) as parallel:    
        r = parallel(delayed(run_worker)(s, worker_script, folder_run, node_list[s]) for s in range(n_jobs))
    for res in r:
        results.append(res)


