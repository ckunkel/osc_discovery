import os, sys, json, pickle, shutil, socket
import numpy as np
from ase.io import read
from rdkit import Chem

from osc_discovery.cheminformatics.cheminformatics_misc import xyz2ase
from osc_discovery.descriptor_calculation.xtb_tools import calculate_single_molecule_properties_xtb
from osc_discovery.cheminformatics.cheminformatics_symmetry import Symmetry
from osc_discovery.acquisition_function import linear_correct_to_b3lyp

from osc_discovery.descriptor_calculation.calc_lambda_gasphase_dft import LambdaGasPhase
from osc_discovery.descriptor_calculation.aims_tools import run_aims_on_mol, get_homo_lumo_energy
from osc_discovery.aims_settings import aims_binary, aims_species, mpiexe

""" Script to calculate lambda and ehomo values for a single system on a specified node
    Note, that a hostname can be passed to execute the heavy calculational workflow part there.
    This is achieved via ssh into the node. The script is actually running on the same execution 
    host as the worker.
    Commandline-Arguments for the script: filename, hostname
"""

# identify cluster, use setting according to nodes
n_cpu='16'; n_cpu_conformer_ranking = 14
if 'SLURM_JOB_PARTITION' in os.environ.keys():
    if 'mpp3' in os.environ['SLURM_JOB_PARTITION']: 
        n_cpu = '24'
        n_cpu_conformer_ranking = 14


if __name__ == '__main__':

    cwd=os.getcwd()

    # Needs a smi file as input
    system_to_calculate=sys.argv[1]
    with open(system_to_calculate) as out: smi=out.read()
    
    # determine hostname to run on
    hostname=sys.argv[2]
    if hostname!='0': mpiexe+=' --hosts {}'.format(hostname)

    # logging jobid
    os.system('touch slurm_{}.txt'.format(os.environ['SLURM_JOBID']))
   
    run_mode='ssh'
    scratchdir=''

    # run actual IO in a scratchdirectory specified by environment variable
    if 'SCRATCH' in os.environ.keys(): #linux cluster
        scratchdir = os.path.join( os.environ['SCRATCH'], system_to_calculate )
   
    xc = 'b3lyp'

    # determine symmetry
    symmetry=Symmetry()(Chem.MolFromSmiles(smi))



    ################################
    # First step: Run conformer search on specified host node

    if run_mode=='local' or hostname=='0':
        # Run GFN1 calcualtion, generate initial conformer
        # This method generates a lot of output so better route it to scratch by
        # setting the environment variable
        atoms_neut_h = calculate_single_molecule_properties_xtb( smi, 
                                                             multiproc_conformers = n_cpu_conformer_ranking, 
                                                             scratchdir = scratchdir )
        atoms_neut_h.write('atoms_neut_xtb_res.xyz')

    else: #ssh, passwordless authentication is necessary
        import spur
        shell = spur.SshShell(hostname=hostname, missing_host_key=spur.ssh.MissingHostKey.accept)
        print('SSH connection established from {} to {}'.format(socket.gethostname(), hostname))
        with shell: 
            script_run = os.path.join('/'.join(os.path.realpath(__file__).split('/')[:-1]), 'calc_lambda_ehomo_gfn1.py')
            print(["python", '-u', script_run, system_to_calculate, n_cpu_conformer_ranking]) 
            result = shell.run(["python", '-u', script_run, system_to_calculate, str(n_cpu_conformer_ranking)], cwd=cwd)
            print(result.output)

    atoms_neut_h = read('atoms_neut_xtb_res.xyz')

    # xtb results if DFT is skipped 
    homo_en = linear_correct_to_b3lyp('ehomo_gfn1_b3lyp',  atoms_neut_h.info['HOMO_en_eV'])
    e_lambda = linear_correct_to_b3lyp('XTB1_lamda_h',  atoms_neut_h.info['e_lambda_h'])



    ########################
    # Second step, run DFT calculations for lowest energy conformer

    if not "XTB_ONLY" in list(os.environ.keys()):
        if scratchdir!='':
            if os.path.isdir(scratchdir): shutil.rmtree(scratchdir)
            os.system('ln -s {} .'.format(scratchdir))
            os.mkdir(scratchdir)
            os.chdir(scratchdir)

        occupation_type = 'gaussian 0.05'
        
        # Run aims B3LYP calculation for lambda
        species_type = 'light'
        calc = LambdaGasPhase(aims_binary,
                          aims_species,
                          xc,
                          atoms_neut_h,
                          n_cpu=n_cpu,
                          occupation_type='gaussian 0.05',
                          preoptimize=True,
                          optimize=True,
                          preoptimize_charged=False,
                          species_name_preoptimize=species_type,
                          species_name_preoptimize_charged=species_type,
                          species_name_optimize=species_type,
                          charge=+1,
                          f_max= 0.025,
                          use_restarts=False, 
                          optimizer_type='internal_aims_bfgs',
                          additional_parameters={'vdw_correction_hirshfeld':'.true.'},
                          mpiexe=mpiexe) 

        e_lambda = calc.run_task()
        print('Finished job, lambda: {}'.format(np.around(e_lambda,2)))
        atoms_neut_h = read('qm_tight/aims.out')

        # cleanup restart files
        os.system("find . -name restart.f* -delete")
        os.system("find . -name exx_* -delete")
    

        # Run aims B3LYP calculation for HOMO/LUMO
        xc='b3lyp'
        n_cpu='16'
        species_type = 'light.ext'
        energy, mol = run_aims_on_mol(atoms_neut_h, aims_add_params={'vdw_correction_hirshfeld':'.true.'}, 
                                  xc_in=xc, label='.', species_in=species_type, ncpu_in=n_cpu, mpiexec=mpiexe)
        res = get_homo_lumo_energy('.')
        homo_en = res['HOMO_energy']
        with open('res_homo.txt', 'w') as out: out.write(json.dumps(res))

        if scratchdir!='': 
            os.chdir(cwd)
            os.system('mv {}/* .'.format(scratchdir))
            shutil.rmtree(scratchdir)
 
    # write to common resultsfile, read by the AML workflow
    os.system('echo "{} {} {} {}" >> ../../results_calculations.txt'.format(system_to_calculate, smi, e_lambda, homo_en))
