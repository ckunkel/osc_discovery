import os, sys, time, socket, datetime, random, subprocess
from order_systems import order_by_datetime as order_fxn
#from order_systems import dont_order as order_fxn

""" Workflow script to execute different a simple script on a larger number of systems
Please adjust folder and workflow settings to run it properly. 

Settings can be made in a section below

Directory structure will then be:

<basedir>
- workflow.py
- run_script1.py
- run_script2.py
- log.txt (workflow log)
- <cif_directory> 
- <results_directory>
 -- <system1>
   --- <run_script1>
   --- <run_script2>
 -- <system2>
   --- <run_script1>
   --- <run_script2>

The status of each run_script execution, is indicated by the suffixes of each folder, either 
  "<run_script1>_running" (a worker is currently executing here), 
  "<run_script1>_fizzled" (a problem occurred in execution)
  "<run_script1>_completed" (calculation finished successfully)
  A similar naming scheme is also used for each system, here an extra state can occur:
  "<system1>_partly_completed" when not all run_scripts were successful (any one fizzled)

Author: Christian Kunkel
"""


if __name__ == '__main__':
        
	# prevent startup problems due to workers writing at same time
	random.seed(datetime.datetime.now())
	sleep_time_startup=3+random.random()*15
	print('Sleeping for {} s'.format(sleep_time_startup))
	time.sleep( sleep_time_startup )
        
	#folder setting
	basedir = os.getcwd()
	if len(sys.argv)>1: cif_directory = os.path.join(basedir, sys.argv[1])
	else: cif_directory = os.path.join(basedir, 'molecules_to_calculate')
	if len(sys.argv)==3: hostname = sys.argv[2]
	else: hostname = '0' 
	results_directory = os.path.join(basedir, "{}_results".format(cif_directory))
        
       
	################### EDIT TO YOUR NEEDs
	# Workflow settings
	max_runs_in_one = 5000000 # Maximum systems to be calculated in one submission
	run_scripts =["calc_lambda_ehomo_b3lyp.py"]
	pythoncommand = ["python", "", "", ""] #scriptname systemfile(.xyz) hostname(0 if not assigned)
	run_infinite = True # this can be used to keep the worker running and waiting for new jobs
	timeout_script = -1 # this can be used to keep the worker running and waiting for new jobs
	###################


	if not os.path.isdir(results_directory): os.mkdir(results_directory)

	time_start_wf = time.time()
	workflow_identifier = datetime.datetime.now()

	# Message interface
	f = open("log{}.txt".format(workflow_identifier), "a")
	def output_message(message):
		global f
		f.write(message)
		pieces = message.split("\n")
		for piece in pieces:
			print(piece)
	
	output_message("\n\n-----------------------\nStarting new workflow {}\n".format(workflow_identifier))
	output_message("   Execution host: {}\n\n".format(socket.gethostname()))

	# Outer loop: Run workflow for every system
	for number_run in range(max_runs_in_one):
	        
		successes_system = 0 # How many run_scripts for this system were successfull
		time_start_system = time.time()

                # here we could implement error handling in the following way: If a system is fizzled a few times only (logfile necessary)
                # then it could be repeated. 

		# Check what has been calculated or is running
		content_resultsdir = [x.split("__")[0] for x in os.listdir(results_directory)]
		systems_cif_dir_to_calc = [x for x in os.listdir(cif_directory)]
		systems_cif_dir_to_calc = list(set(systems_cif_dir_to_calc) - set(content_resultsdir))
		systems_cif_dir_to_calc=order_fxn(systems_cif_dir_to_calc, folder_systems=cif_directory)
		
		if len(systems_cif_dir_to_calc) == 0:   # Everything finished?
			if run_infinite: time.sleep(100); continue
			output_message("\nNo more systems to calculate, workflow {} finished\n--------------------------\n\n".format(workflow_identifier))
			os.chdir(results_directory)	
			time_end_wf = time.time()
			os.system('echo "\n\n"Worfklow {} >> runtimes_workflows.txt'.format(workflow_identifier, time_end_wf - time_start_wf))
			os.system('echo {} >> runtimes_workflows.txt'.format(time_end_wf - time_start_wf))
			sys.exit(0)

		elif os.path.isfile(os.path.join(cif_directory, 'stop.txt')):   # Everything finished?
			output_message("\nStopping as requested by stop.txt\n")
			sys.exit(0)

		else:   # If not, proceed to next system
			system_to_calculate = systems_cif_dir_to_calc[0]
			path_system_to_calculate = os.path.join(cif_directory, system_to_calculate)
			os.mkdir(os.path.join(results_directory, system_to_calculate+"__running"))
			output_message("\n-------------------\nRunning system {}\n".format(system_to_calculate))
	
		
		# Inner loop: Execute every script on the system
		for script in run_scripts:
			
			time_start_script = time.time()
			success_script = False # Whether this run script was successful
			output_message("Starting wf step {}\n".format(script))
			path_to_script = os.path.join(basedir, script)
			results_subfolder = script.split(".")[0]
			
			# Script works with softlinks in the folders
			os.mkdir(os.path.join(results_directory, system_to_calculate+"__running", results_subfolder+"__running"))
			os.chdir(os.path.join(results_directory, system_to_calculate+"__running", results_subfolder+"__running"))
			os.system("ln -s {} .".format(path_to_script))
			os.system("ln -s {} .".format(path_system_to_calculate))

			
			try:    # Execute worker and test for success 
				pythoncommand_run = pythoncommand
				pythoncommand_run[1] = script
				pythoncommand_run[2] = system_to_calculate
				pythoncommand_run[3] = hostname
				print( pythoncommand_run[3] )
				if timeout_script > 0: subprocess.run( pythoncommand_run, check=True, timeout=timeout_script )
				else: subprocess.run( pythoncommand_run, check=True )
				success_script = True
			except Exception as e: 
				success_script = False
				os.system('echo "{}" >> timeouts.txt'.format(pythoncommand_run))
				os.system('echo "{}" >> timeouts.txt'.format(e))
				print('Timeout or error')

			
			#try:    # Execute worker and test for success 
			#	if not os.system(pythoncommand.format(script, system_to_calculate, hostname)):
			#		success_script=True
			#except: 
			#	success_script=False

			time.sleep(2)
			time_end_script = time.time()
			
			output_message("Runtime wf step: {}\n".format(time_end_script - time_start_script))
			if success_script:  # Handling success completion
				output_message("Finishing wf step {}\n".format(script))
				successes_system += 1
				state_new = "completed"
			else:  # Handling error state
				output_message("Error in wf step {}\n".format(script))
				state_new = "fizzled"
			os.system('echo {} > runtime_script.txt'.format(time.time() - time_start_script))
			os.chdir(os.path.join(results_directory,system_to_calculate+"__running"))
			os.system("mv {0}__running {0}__{1}".format(results_subfolder, state_new))
			
		time_end_system = time.time()
		output_message("Runtime {}: {}\n".format(system_to_calculate, time_end_system-time_start_system))
		os.system('echo {} > runtime_system.txt'.format(time_end_system - time_start_system))
		output_message("System {} finished successfully".format(system_to_calculate))
		os.chdir(results_directory)
		if successes_system == len(run_scripts):
			state_new = "completed"
		elif successes_system==0:
			state_new = "fizzled"	
		os.system("mv {0}__running {0}__{1}".format(system_to_calculate,state_new))
