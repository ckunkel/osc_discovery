# AML organic semiconductor discovery

As described in the article: [Active Discovery of Organic Semiconductors](https://www.google.com/search?channel=fs&q=Active+Discovery+of+Organic+Semiconductors) (Nat. Comm.)

The main code is written in python and can be accessed as a module after installation. Scripts and command line utilities for important tasks are included, while figures can be recreated from included jupyter notebooks. An archive containing the associated source data is provided in the supplementary materials of the article. 

Author: Christian Kunkel (christian.kunkel@tum.de)

## Installation

We recommend installation of all dependencies in a conda environment (tested with miniconda3):
``` 
    conda config --add channels conda-forge
    conda create --name env1 --file requirements.txt
    conda activate env1   
    python setup.py install 
    unzip data.zip 
```

In addition, JupyterLab can be installed to visualize the provided jupyter notebooks `conda install -c conda-forge jupyterlab`.

To run DFT production calculations with [FHI-aims](https://aimsclub.fhi-berlin.mpg.de/), paths to a binary and to the basis set (species_defaults) must be set in `aims_settings.py`. Also, please install aimsutils (https://gitlab.lrz.de/theochem/aimsutils) using `python setup.py install`. In addition, the xTB code needs to be callable as `xtb`, find it on [github](https://github.com/grimme-lab/xtb).

Note: The light.ext basis set files can be generated from the light default basis sets by 
      uncommenting the following basis functions:
      
``` 
    H (tier 2 hydro 1 s, 2 p) 
    C (tier2 hydro 4 f, 3 p, 3 s) 
    N (tier2 hydro 4 f, 3 p, 1 s)
    O (tier2 hydro 4 f, 3 p, 3 d)
    S (tier 1 hydro 4 f, tier 2 hydro 4 d) 
```

Mainly tested on unix based HPC systems (here, the [LRZ-LINUX cluster](https://doku.lrz.de/display/PUBLIC/Linux+Cluster) segments CoolMuc-2, or 3 with SLURM for job scheduling). For an AML run on the molecular test space, single or shared nodes with sufficient memory and number of cores were used. For the DFT-B3LYP production runs, additional processing capability is needed. The provided jobscripts for descriptor calculation (in `worker_package_b3lyp`) are adapted to the LRZ HPC system, and use ssh connections to distribute jobs among compute nodes. 
      
## Structure

`scripts` contains all command line utilities to carry out the AML workflow or to generate the molecular testspace.

`figures` contains jupyter notebooks to recreate the figures, using the files from the `data` directory.

`worker_package_b3lyp` contains utilities to execute the descriptor-calculation workflow on the HPC system. 

`osc_discovery` contains the python module to execute the AML workflow, to handle GPR, cheminformatics tasks and descriptor calculations. 

The archive containing the source data should be unpacked, and the created folder `data` be placed in this directory. `data` contains all files necessary to reproduce the figures in the article, including:

- `df_chemical_space_chons_4rings.json` - Molecular test space (pandas dataframe).

- `graph_molecular_testspace.graphml` - Graph layout for the molecular testspace used in Figs. 2, 3. 

- `df_initial_gfn1_testspace.json` - Initial population in the limits of the molecular test space and xTB-GFN1 descriptor values (pandas dataframe).

- `df_initial_b3lyp_unlimited.json` - Initial population in the virtually unlimited molecular space, containing DFT-B3LYP descriptor data (pandas dataframe).

- `df_typical_oscs.csv` - Descriptor values for the set of typical OSC materials, see Supplementary Figure 1,4.

- `df_population_b3lyp_run.json, df_population_b3lyp_run_SI.json` - Results of the DFT-production runs of Fig. 4 and Supplementary Figure 10, 11 (pandas dataframes).

- `runs_*` - AML runs carried out in the molecular test space. Contains source data for Supplementary Figures 6,7,8,9 and -Table 1

- `df_conformers_random.json` - Conformer-assessment, Supplementary Figure 5. 

## Running calculations in the molecular test space

In the scripts folder call e.g. `python active_learner_run.py --kappa 2.5 --n_batch 100 --two_fold 0 --n_learning_steps 50`. The help context menu provides detailed information for all options ( call `python active_learner_run.py --help` ). Example outputs for different run types are given in the data directory, see above. For production runs at the DFT-B3LYP use the `--use_reference_frame 0` option. Note: You might need to adjust the settings in `active_learner_run.py` according to your system.

## License

[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0)
