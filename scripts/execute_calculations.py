import sys, os, shutil
import pickle
import pandas as pd
import multiprocessing as mp

from ase import Atoms
from ase.io import read
from rdkit import Chem

from osc_discovery.misc import ase2xyz
from osc_discovery.descriptor_calculation.xtb_tools import calculate_single_molecule_properties_xtb

''' Script to build a reference frame after descriptor calculation '''

def calculate_single(smi):
       
    """ Single system evaluation """

    global smiles_set_idx
    idx = smiles_set_idx[smi]
    os.mkdir('run_{}'.format(idx))
    os.chdir('run_{}'.format(idx))
    atoms_res=calculate_single_molecule_properties_xtb(smi, max_conformers=50, 
                                                      method='GFNff', n_proc_each=1)
    os.chdir('..')
    shutil.rmtree('run_{}'.format(idx))
    atoms_res.info['smi']=smi
    atoms_res.info['idx']=idx
    atoms_res.write('mol_res_neut_{}.xyz'.format(idx))   

    
if __name__=='__main__':
  
    cwd=os.getcwd()
    folder=sys.argv[1]
    os.chdir(folder)
    folder=os.getcwd()
    lines_read_in=[]
    
    # Get unique list of smiles and calculate
    list_gen_dir=[x for x in os.listdir('.') if 'gen_' in x]
    list_gen_dir=['gen__{}.smi'.format(i) for i in range(len(list_gen_dir))]

    smiles=[]    
    for filex in list_gen_dir:
        print(filex)
        with open(filex) as out:
            for line in out.readlines():
                smiles.append(line.split()[0])
    print('Number of smiles: {}, Unique: {}'.format(len(smiles), len(set(smiles)) ))
    smiles_set=sorted(list(set(smiles)))
    
    # get unique idx for processing
    smiles_set_idx={}
    for idx,smi in enumerate(smiles_set):
        smiles_set_idx[smi]=idx
        
    # remove those were previously processed
    smiles_processed=[]
    if os.path.isdir('res_previous_calcs'):
        for i,filename in enumerate([x for x in os.listdir('res_previous_calcs') if '.xyz' in x]):
            atoms_obj=read(os.path.join('res_previous_calcs',filename))
        
            with open(os.path.join('res_previous_calcs',filename)) as out:
                lines=out.readlines()
                smi=lines[1].split('smi=')[1].split()[0]
            
            #if atoms_obj.info['err']: continue
            smiles_processed.append(smi)
    smiles_set_to_process=sorted(list(set(smiles_set)-set(smiles_processed)))
   
    print('Number of smiles to process: {}'.format(len(smiles_set_to_process)))
 
    # Process in scratch
    if not os.path.isdir('/scratch/kunkel/calculations'): os.mkdir('/scratch/kunkel/calculations')
    os.chdir('/scratch/kunkel/calculations')
    pool = mp.Pool(processes=32)
    res_atoms = pool.map(calculate_single, smiles_set_to_process)
    pool.close()
    
    # copy results back
    os.chdir(cwd)
    os.chdir(folder)
    
    
   
