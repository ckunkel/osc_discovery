import os, sys, pickle, argparse, socket
import pandas as pd
from osc_discovery.learners_treesearch import *
from osc_discovery.acquisition_function import linear_correct_to_b3lyp
from rdkit import rdBase


""" Script to run AML discovery """

# remove output of sanitation errors, those are redirected to sanitation_errors.txt
rdBase.DisableLog('rdApp.error')
cwd = os.getcwd()


# determine system we are running
if os.path.isdir('/gpfs/scratch/pr47fo/ga62yex2/ga62yex2/'):
    #LRZ Linux cluster (SLURM based system)
    system = 'LRZ_LINUX_CLUSTER'
    dir_scratch = '/gpfs/scratch/pr47fo/ga62yex2/ga62yex2/'
    worker_submit_file='submit_lrz.sh'
    submit_command = 'sbatch'
    run_mode = 'queue'
    queue_status_command = 'squeue --cluster=cm2_tiny'

else:
    # our local cluster (PBS based system)
    run_mode = 'queue'
    if run_mode == 'local':
        worker_submit_file = 'submit_local.sh'
    else:
        worker_submit_file = 'submit_arthur.sh'
    system = 'ARTHUR'
    dir_scratch = '/scratch/kunkel/'
    submit_command = 'qsub'
    queue_status_command = 'qstat'




if __name__=='__main__':

    # Get settings from commandline, some might be overwritten if restart of a run is requested.
    parser = argparse.ArgumentParser(description='Script to run AL discovery')
    parser.add_argument('--kappa', metavar='kappa', type=float, default=2.5,
                        help="Kappa value, as discusse--two_fold in article" )
    parser.add_argument('--n_batch', metavar='n_select_every_step', type=int, default=100, 
                        help="Nbatch as discussed in the article")
    parser.add_argument('--two_fold', metavar='two_fold', type=int, default=0,
                        help="""If no reduction in space is used, this is a switch between
                                one- or two-time exhaustive application of all morphing operations""")
    parser.add_argument('--use_reference_frame', metavar='use_reference_frame', type=int, default=1,
                        help="""This switch determines, wether a benchmark run in the limited molecular testpace is performed
                              relying on descriptor values provided, or wether a production run in an 
                              unlimited molecular space is actually performed at the DFT-B3LYP level of theory.""")
    parser.add_argument('--n_learning_steps', type=int, default=50, 
                        help="Number of learning steps n to be carried out")
    parser.add_argument('--restart_folder', type=str, default='', help="Restart a calculation in this folder")
    parser.add_argument('--suffix', type=str, default='', help="""A suffix to the folder name, has no algorithmic meaning,
                                                                  but can be used to set a name. """)
    parser.add_argument('--random_state', type=int, default=42, 
                         help="Random number generater initialization")
    parser.add_argument('--reduced_search_space', type=int, default=0, help="Switches on reduction of search space.")
    parser.add_argument('--depth_search', type=int, default=3, help="d_search as discussed in the article.")
    parser.add_argument('--Ndeep', type=int, default=500, help="N_deep as discussed in the article.")

    # Command line settings are valid
    args = parser.parse_args()
    properties=["XTB1_lamda_h", "ehomo_gfn1_b3lyp"]
    # We are setting up a new calculation
    if args.restart_folder=='':

        # This is the evaluation in a predefined chemical space, without dft evaluation
        if bool(int(args.use_reference_frame)):
            
            # initial generation dataframe, contains mols and descriptors, already contains B3LYP corrected xTB-GFN1
            df_initial_population = pd.read_json('../data/df_initial_gfn1_testspace.json', orient="split") 
            
            df_reference = pd.read_json('../data/df_chemical_space_chons_4rings.json', orient="split") #reference frame
            print(df_reference)
            preset_chemical_space_boundaries = "test_osc_space"
            n_worker_submit = 1 # How many workers to submit
            n_select_every_step = int(args.n_batch)
            n_execute_every_step = 0 # 0 means all cases are always found
            

        # this is the real production run with DFT-evaluation
        else: 
            df_reference=[]
            df_initial_population=pd.read_json('../data/df_initial_b3lyp_unlimited.json', orient="split")
            n_worker_submit = 8
            preset_chemical_space_boundaries = '' # unlimited
            
            # Settings from paper
            args.kappa = 2.5
            args.reduced_search_space = 1
            args.two_fold = 0
            args.depth_search = 3
            args.Ndeep = 500
            args.Ndeep = 500
            args.n_learning_steps = 50
            n_execute_every_step = 100 #int(args.n_batch) # 0 means all cases are always found
            n_select_every_step = 200


        if int(args.reduced_search_space): rws_descriptor = '{}-{}-{}'.format(args.reduced_search_space, 
                                                                              args.depth_search, args.Ndeep)
        else: rws_descriptor = args.reduced_search_space
        dir_save_results = 'active_learner_results_script_twofold_{}_kappa_{}_nbatch_{}_runs_{}_production_{}_rwssearch_{}_suffix_{}'.\
                          format(args.two_fold, args.kappa, args.n_batch, args.n_learning_steps, 
                                 not args.use_reference_frame, rws_descriptor, args.suffix)

        # For security, we dont want to override our run.
        if not args.use_reference_frame: 
            if os.path.isdir(dir_save_results): 
                print('Folder exists... aborting ...')
                sys.exit(1)

        print('Output folder', dir_save_results)

        # redirecting stdout to logfile
        stdout=sys.stdout
        sys.stdout = open('two_fold_{}_kappa_{}_nbatch_{}__runs_{}_production_{}_suffix_{}.log'.format(args.two_fold, 
                           args.kappa, n_select_every_step, 
                           args.n_learning_steps, not bool(int(args.use_reference_frame)), args.suffix), 'w')

        AL = active_learner(df_initial_population = df_initial_population,
                            n_worker_submit = n_worker_submit,
                            Nbatch_first = n_select_every_step,
                            Nbatch_finish_successful = n_execute_every_step,
                            run_mode = run_mode, 
                            worker_submit_file = worker_submit_file,
                            submit_command = submit_command,
                            queue_status_command = queue_status_command,
                            two_generations_search = int(args.two_fold),
                            df_reference_chemical_space = df_reference, 
                            kappa = float(args.kappa), 
                            reduced_search_space = int(args.reduced_search_space),
                            depth_search = int(args.depth_search),
                            Ndeep = int(args.Ndeep),
                            preset = preset_chemical_space_boundaries,
                            dir_save_results = dir_save_results,
                            dir_scratch = dir_scratch,
                            ml_model = 'gpr_tanimoto_kernel',
                            random_state=int(args.random_state),
                            suffix=args.suffix)

        pickle.dump(AL, open('AL.pkl','wb'))
        res_dicts=[]
        if args.use_reference_frame==0:
            print('Sleeping for 60 seconds...')
            time.sleep(60)

    # Restart capability
    else:
        # remove output of sanitation errors, those are redirected to sanitation_errors.txt
        rdBase.DisableLog('rdApp.error')
        os.chdir(args.restart_folder)
        
        if bool(int(args.use_reference_frame)): res_dicts = pickle.load(open('res_dicts.pkl','rb')) 
        AL = pickle.load(open('AL.pkl','rb'))
        np.random.seed(AL.random_state)

        if os.path.isdir(AL.dir_scratch):
            shutil.rmtree(AL.dir_scratch)
        os.mkdir(AL.dir_scratch)
        os.system('ln -s {} scratch_distance_matrix'.format(AL.dir_scratch))

        AL.run_calculations_population()

        stdout=sys.stdout
        sys.stdout = open(AL.dir_save_results+'.log', 'a')
        print('Restarting run....')
        

    for i in range( int(args.n_learning_steps) +1 ):
        while True:
            if AL.check_all_calculations_finished(): break
            else:
               AL.run_calculations_population()
               print('not finished, waiting...')
               time.sleep(30)
 
        AL.select_and_add_new_candidates()
        
        if args.use_reference_frame==0: 
            pickle.dump(AL, open('AL_{}.pkl'.format(i),'wb'))
        else: 
            pickle.dump(AL, open('AL.pkl','wb'))

        if bool(int(args.use_reference_frame)):
            res_dicts.append(AL.evaluate_performance_external_set())
            pickle.dump(res_dicts, open('res_dicts.pkl','wb'))
            print(res_dicts[-1])
    print('finished')
    print('')

    os.system('touch finished.txt')    
    os.chdir(cwd)
    sys.stdout=stdout
