import sys
sys.path.append('../')
sys.path.append('../morphing_operations')
sys.path.append('.')

import osc_discovery.cheminformatics.cheminformatics_mutation as cheminformatics_helpers_v2
from osc_discovery.morphing_typical_OSC_design import operations_typical_osc_design as operations
from osc_discovery.cheminformatics.cheminformatics_structure_check import check_rules_fulfilled
from osc_discovery.cheminformatics.cheminformatics_symmetry import Symmetry
from osc_discovery.cheminformatics.cheminformatics_misc import flatten_list

import time, os, pickle
import numpy as np
import pandas as pd
import multiprocessing as mp

import rdkit
print(rdkit.__version__)
from rdkit import rdBase,Chem
from rdkit.Chem import AllChem,Draw
from rdkit.Chem.Draw import IPythonConsole
from rdkit.Chem import rdMolDescriptors
from rdkit import RDLogger
lg = RDLogger.logger()
lg.setLevel(RDLogger.CRITICAL)

''' Script to enumerate full chemical space within bounds on molecular size '''

# Check 
failed = 0
stopped = 0
new_generation = 0
allow_only_graph_symmetric = True
multi_proc = 24

cheminformatics_helpers_v2.operations = operations
cheminformatics_helpers_v2.failed = 0
cheminformatics_helpers_v2.stopped = 0
cheminformatics_helpers_v2.new_generation = 0
cheminformatics_helpers_v2.allow_only_graph_symmetric = True
cheminformatics_helpers_v2.multi_proc = multi_proc
cheminformatics_helpers_v2.preset_structure_check='test_osc_space'
cheminformatics_helpers_v2.only_use_brute_force_code=False

mol_test=Chem.MolFromSmiles('c1ccccc1')
check_rules_fulfilled(mol_test, '',  cheminformatics_helpers_v2.preset_structure_check ,verbose=True)


def run_generation_text_based(textfile=''):
        
    template_save='{}__{}__{}'
    dict_smi_list_mutate={}
    
    print(textfile)   
 
    # generate a uniqe dict
    if textfile=='':
        dict_smi_list_mutate['c1ccccc1']='0__none__none'
        cheminformatics_helpers_v2.last_generation=0
        with open('gen__0.smi','w') as out:
            out.write('c1ccccc1 0__none__none')
            
    else:
        cheminformatics_helpers_v2.last_generation = int(textfile.split('__')[-1].split('.smi')[0])       
        i=0
        dict_smi_list_file={}
        with open(textfile) as out:
            for i,line in enumerate(out.readlines()):
                line=line.split()
                dict_smi_list_file[line[0]]=line[1]

            print('Number of lines: {}'.format(i))

        already_mutated=[]
        for i in range(cheminformatics_helpers_v2.last_generation):
            filex='gen__{}.smi'.format(i)
            print(filex)
            with open(filex) as out:
                for line in out.readlines():
                    already_mutated.append(line.split()[0])
                    
        to_mutate = set(dict_smi_list_file.keys()) - set(already_mutated)
        for m in list(to_mutate):
            dict_smi_list_mutate[m]=dict_smi_list_file[m]
                    
    print('Unique molecules to mutate: {}'.format(len(dict_smi_list_mutate.keys())))
    
    cheminformatics_helpers_v2.new_generation = cheminformatics_helpers_v2.last_generation + 1
    cheminformatics_helpers_v2.mols_last_generation_smiles = list(dict_smi_list_mutate.keys())
    textfile_new_gen='gen__{}.smi'.format(cheminformatics_helpers_v2.new_generation) 
    
    pool = mp.Pool(processes=multi_proc)
    new_frames = pool.map(cheminformatics_helpers_v2.run_mutation, list(dict_smi_list_mutate.keys()) )
    pool.close()

    new_frames_scratch=[]
    for framex in new_frames:
        for frame in framex:
            new_frames_scratch.append(frame)
    new_frames=new_frames_scratch
    
    ##new_frames=list(flatten_list(new_frames))
  
    with open(textfile_new_gen, 'w') as out:
        for i,f in enumerate(new_frames):
            if len(f.molecule_smiles.values[0])==0: continue
            # format: smi gen_nr__mol_smi_lastgen__op
            out.write('{} {}__{}__{}\n'.format(f.molecule_smiles.values[0], cheminformatics_helpers_v2.new_generation, 
                                           f.molecule_last_gen.values[0], f.operation.values[0]))
    
    statistics = [len(new_frames), cheminformatics_helpers_v2.stopped, cheminformatics_helpers_v2.failed]
    return statistics, textfile_new_gen



if __name__=='__main__':

    time_start_total=time.time()
    
    ratios=[]
    n_mols_unique=[]
    fname=sys.argv[1]
    
    for i in range(100):
    
        time_start=time.time()
        statistics, fname = run_generation_text_based(fname)
        print('Time for step: {}'.format(time.time()-time_start))
        print('New lines: {}, Stopped: {}, failed: {}'.format(*statistics))
        print('')
        print('')
        
    print('Total time: {}'.format(time.time()-time_start_total))

    
    
    
    
