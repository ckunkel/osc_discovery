from distutils.core import setup

setup(
    name='osc_discovery',
    version='',
    packages=['osc_discovery', 'osc_discovery.descriptor_calculation', 'osc_discovery.cheminformatics'],
    url='',
    license='',
    author='Christian Kunkel',
    author_email='christian.kunkel@tum.de',
    description=''
)
